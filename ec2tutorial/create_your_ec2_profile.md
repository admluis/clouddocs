# Creating your EC2 profile

These steps describe how to create your environment using the Amazon EC2 compatible commands from the [eucatools suite](http://www.eucalyptus.com/docs).

## Obtaining the EC2 zip profile

The  euca2ools require a set of shell environment variables in order to run. These variables can be obtained and stored in an 'rc' file much like .profile when logging into a linux server. The environment variables are different per project that you work on.

The profile can be selected through the dashboard at http://cern.ch/openstack.

  * Select the 'Access and Security' menu item
  * Select the 'API Access' tab
  * Select 'Download EC2 credentials' button on the top right

This will download a file called Personal-<i>userid</i>-x509.zip. You should then transfer this to your lxplus account using 
a tool like scp or via AFS.

## Unzip the EC2 key zip file

On lxplus, you can unpack the zip file to create the appropriate key files and the profile ```ec2rc.sh```.

```
$ unzip "Personal timbell-x509.zip"
Archive:  Personal timbell-x509.zip
 extracting: pk.pem
 extracting: cert.pem
 extracting: cacert.pem
 extracting: ec2rc.sh```

## Testing the credentials

The images can be listed using euca-describe-images.

```
$ . ec2rc.sh
$ euca-describe-images
IMAGE   ami-00000002    None (SLC6 Server - i386 [130624])      29d90dc8-2558-41c9-a997-836c0d1df2fb    available       public          i686    machine  instance-store
IMAGE   ami-00000003    None (SLC6 Server - x86_64 [130624])    29d90dc8-2558-41c9-a997-836c0d1df2fb    available       public          x86_64  machine  instance-store
IMAGE   ami-00000004    None (SLC5 Server - x86_64 [130624])    29d90dc8-2558-41c9-a997-836c0d1df2fb    available       public          x86_64  machine  instance-store
IMAGE   ami-00000036    None (Windows Server 2008 R2 - x64 [130904])    29d90dc8-2558-41c9-a997-836c0d1df2fb    available       public          x86_64  machine                           instance-store
IMAGE   ami-00000040    None (SLC6 CERN Server - i386 [130920]) 29d90dc8-2558-41c9-a997-836c0d1df2fb    available       public          i686    machine  instance-store```





