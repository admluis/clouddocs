# EC2 Tutorial

The CERN cloud also supports a subset of the EC2 API as used by Amazon and with the Eucalyptus tool suite commonly called euca2ools. Not all of the functions of the OpenStack API are available through this interface and not all of the EC2 functions are supported by OpenStack but if you are very familiar with EC2, this may be an option for you.

For new users, it is recommended to use the OpenStack tools. These tend to be more functional and more widely used by OpenStack users.

This tutorial can be run on lxplus or on a machine with the euca2ools package installed.
