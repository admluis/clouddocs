# Using eucatools to create a VM

This page describes how to create a VM using eucatools. The pre-requisities are

  * [Setting up your EC2 profile](http://clouddocs.web.cern.ch/clouddocs/ec2tutorial/create_your_ec2_profile.html)
  * [Setting up your key pair](http://clouddocs.web.cern.ch/clouddocs/tutorial/create_your_openstack_profile.html#keypair), assuming it is lxplus in this documentation.

The detailed differences between the command line tools are covered in the [comparison](http://clouddocs.web.cern.ch/clouddocs/using_openstack/README.html) section.

  * Use ```euca-describe-images``` to find the image you want. Images have names like ami-0000002f. This is then used when the image is started.
  * A flavor needs to be selected. The best way of listing the flavors seems to be to use the dashboard or the ```openstack flavor list``` command. The Amazon names are well known, being of the format m1.*

The VM can then be created as follows

```
$ euca-run-instances ami-000002fa -t m1.tiny -k lxplus
RESERVATION     r-lev2y3lb      841615a3-ece9-4622-9fa0-fdc178ed34f8    default
INSTANCE        i-000c446b      ami-000002fa            server-43265d33-50d6-4ea1-a306-f5ffcbe8ebba     pending lxplus  0               m1.tiny 2014-12-15T14:29:02.000Z  nova                            monitoring-disabled                                     instance-store```

The hostname is ``` server-43265d33-50d6-4ea1-a306-f5ffcbe8ebba``` and cannot be overridden.

The VM build process can be following using ```euca-describe-instances```.

```
euca-describe-instances i-000c446b```

