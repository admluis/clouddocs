# VM placement

## Availability zones

When an application has been made cloud-aware and it can tolerate single VM failure, 
availability can be further improved by placing VMs in different parts of the data centres.

An availability zone is a way in which the user can specify a particular “location” 
in which a host should boot. The most common usage for availability zones is to 
group together servers in terms of availability but other features could be external 
network connectivity or redundant power. When a nova boot command is issued, an 
```--availability-zone``` can be provided to specify which zone to start the VM in.

The current zones available can be found running ```nova availability-zone-list```. 
The results show the name of the availability zones currently available such as

```
$ nova availability-zone-list
+---------------+-----------+
| Name          | Status    |
+---------------+-----------+
| cern-geneva-a | available |
| cern-geneva-b | available |
| cern-geneva-c | available |
+---------------+-----------+
```

Each of these zones have different local network switch and power inputs. Thus a 
failure in one hardware component of the cern-geneva-a should not affect VMs running in cern-geneva-b.

To create a VM in a particular availability zone, this should be specified at the ```nova boot``` stage

```
nova boot --flavor m1.small --image centos7 --key-name lxplus --availability-zone cern-geneva-a timgvaa
```

An additional availability zone called ```nova``` is used where dedicated servers 
and special configurations are used.

## Data centre selection

For disaster recovery scenarios, it may be necessary to select the data centre 
where the virtual machine should be scheduled. This can be specified using the 
datacenter property.

For details, see the [data centre properties settings](http://clouddocs.web.cern.ch/clouddocs/using_openstack/properties.html).