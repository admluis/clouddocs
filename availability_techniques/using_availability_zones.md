# Using availability zones

The [Availability zones](http://clouddocs.web.cern.ch/clouddocs/using_openstack/availability_zones.html) feature allows virtual machines to be placed into particular locations in the computer centres.

To achieve high availability using availability zones, a set of VMs are distributed into different zones which are largely independent of each other.

```
$ nova availability-zone-list
+---------------+-----------+
| Name          | Status    |
+---------------+-----------+
| cern-geneva-a | available |
| cern-geneva-b | available |
| cern-geneva-c | available |
| cern-wigner-a | available |
| cern-wigner-b | available |
+---------------+-----------+
```