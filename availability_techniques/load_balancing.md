# Load balancing

Load balancing is a technique where multiple virtual machines can be used to provide a single service. This can be done through a variety of techniques.

A pre-requisite is a set of identically configured machines. This can be achieved using Configuration management which provides a mechanism to create a system with a rule defined configuration and maintain it consistently.  For services on the CERN cloud, the recommended configuration management system is Puppet using the configuration management framework and tools documented in the [configuration User Guide](http://cern.ch/configdocs).

# LanDB Aliases

The simplest approach is to use a LanDB alias. This allows a service hostname to be given such as *myservice.cern.ch* which is defined to point to a virtual machine such as *myservice01.cern.ch*. This is done via the meta data option *landb-alias* as defined in [VM properties](../using_openstack/properties.md).
http://configdocs.web.cern.ch/configdocs/dnslb/README.html)
In the event of a failure, this alias could be updated online to a different server, such as *myservice02.cern.ch*. After a short refresh period (around 15 minutes), all new accesses to *myservice.cern.ch* would go to the new server.

The update can be performed as follows

```
openstack server unset --property landb-alias myservice01.cern.ch
openstack server set --property landb-alias=myservice myservice02.cern.ch```

## DNS Load balancing

CERN IT runs a load balancing service which allows multiple machines to be registered behind a single DNS name. This is used in cases such as lxplus to allow VMs to be moved in and out of service. The state of the machines can be managed using [various CERN tools](http://configdocs.web.cern.ch/configdocs/state/README.html) and is integrated with the CERN monitoring and alarming procedures.

The DNS load balanced configurations are integrated with the configuration management tools such as Puppet and Roger. For details of how to set one up, see the [Configuration chapter
on Load Balancing]. Along with how to configure the DNS load balancer, this also explains operations such as removing and adding VMs from the set of machines.

## HA Proxy

The [HA Proxy](http://www.haproxy.org/) software provides a socket level load balancer.