# Availability Techniques

One of the basic principles of cloud computing is that infrastructure can go wrong. The computer centre infrastructure, the hardware, the operating system, the network can all have failure scenarios. With cloud computing and a modern application architecture, these failure scenarios are handled at the application level by some of the techniques described in this chapter such as

   * [Restarting VMs on operating system failure](http://clouddocs.web.cern.ch/clouddocs/availability_techniques/reboot_vm_on_failure.html)
   * [Load balancing](load_balancing.md)
   * Placing virtual machines in different [availability zones](http://clouddocs.web.cern.ch/clouddocs/using_openstack/availability_zones.html) of the computer centre


