# Requesting quota for your shared project

If you would like to request quota changes for your shared project, please fill out a [form in the service portal](https://cern.service-now.com/service-portal/report-ticket.do?name=cloud-quota&fe=cloud-infrastructure) providing the following information:

| Field | Description |
| ----- | ----------- |
| Project name | The project for which you want to change the quota. Note: only project members are allowed to request such changes. |
| Virtual Machine quota | Instances, Cores, RAM. All values are optional. |
| Block Storage quota | Diskspace, volumes. All values are optional. |

Note: quota changes can only be requested for shared projects. No quota changes can be applied on personal projects.