# Creating projects

Projects are intended to group related virtual machines into a single set with shared administrators and quota. This provides an alternative to the per-machine hardware/VM request form and grants the right to use a set of virtual resources up to a quota limit.

Each user who subscribes to the cloud service is given a personal project. Personal projects are good for sandboxes to test ideas or virtual desktops (such as needing multiple different linux environments to try out different software but with a Mac laptop as a client)

A project would generally correspond to a particular service such as CMS Frontier or IT Twiki.

Using department group names such as IT OIS is to be avoided as re-organisations lead to out of date names and incorrect role assignments. Thus, staying with a small grain functional area is better than large scale projects for a particular department or group.

If you need a new project, please use the [form in the service portal](https://cern.service-now.com/service-portal/report-ticket.do?name=cloud-service&fe=cloud-infrastructure) providing the following information:

| Field | Description |
| ----- | ----------- |
| Experiment or department | Which organisation will be managing the project. This also determines the appropriate usage and utilisation tracking |
| Project name|The name describing the project. Project names can include spaces. CERN projects are generally two or more words like "IT LFC" with High level organisation unit (such as ATLAS, CMS or IT) .. case is significant so please use standard capitalisation|
|Description|Small (one or two lines) description of the project|
|Project members|The e-group or accounts which will be members of the project. Using an e-group is recommended since it allows additional members to be added without requiring further service tickets. These people will be able to create/delete/start/stop virtual machines for the shared project. The membership can be adjusted afterwards through the accounts portal |
|Owner|The primary account name of the person owning this project.<br> <br>**Note:** this account should already exist in the Cloud Infrastructure service.|
|Virtual Machine Quota|Requested resources. In general, a ratio of 1 core to 20GB of disk space corresponds to the flavors of VMs which are available.<p><ul><li>number of virtual machines<li>number of cores<li>amount of memory [GB]<li>amount of disk space [GB]</ol>.You can also request that the hep2.* flavors are added as defined in [Flavors](http://clouddocs.web.cern.ch/clouddocs/details/flavors.html)|
|Volume Quota|Attached disk [volumes](http://clouddocs.web.cern.ch/clouddocs/details/block_volumes.html) for the project. By default, these volumes are provided with standard storage type.|
|Comments|Please note any specific requirements you have in this field such as Wigner vs Meyrin resources|

As part of the request work flow, projects will be reviewed by the LHC resource co-ordinator.

As described in upcoming enhancements, there will be a method to group projects together into domains which will be defined at the experiment level. Thus, smaller projects are recommended so this implementation can be made when the function is available.