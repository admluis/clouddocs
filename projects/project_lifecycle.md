# Project lifecycle
In the Cloud Infrastructure service, there are two types of projects, either a personal project or a shared project.

Both types of projects are integrated into the CERN Resource Lifecycle Management and they have a different lifecycle. The diagram below shows what happens to these different projects when the owner leaves.

![](http://cern.ch/clouddocs/images/project_lifecycle.jpeg)

When a user leaves CERN, all shared projects are assigned to his/her supervisor automatically. The personal project is left untouched.

3 months later, when the primary account of the user is disabled, the personal project gets disabled as well. All instances are stopped and the access to the project is closed.

6 months after departure, the primary account of the user is deleted, and his/her personal project gets deleted. All instances, images and volumes are also deleted.

**Note: ** A user can transfer ownership of a Shared Project at any time on the [Resources ](http://cern.ch/resources) website.

If a user no longer needs his/her shared project, he/she can request a deletion by opening a ticket using the [form in the service portal](https://cern.service-now.com/service-portal/report-ticket.do?name=cloud-service&fe=cloud-infrastructure).
