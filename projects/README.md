# Projects

Projects are groups of related virtual machines such as those concerning a specific activity or production service.

All users have their own personal project created with a small quota. This is to allow testing and VMs which will not need to be shared with other users such as a Windows virtual desktop for a user who has a Mac or Linux client and occasionally needs Windows. The quota on these personal projects is set to a small level as generally these are only intended for personal use. The quota for personal projects is fixed so if additional resources are required, a shared project should be requested. The only flavors available on a personal project as the m1.* series. Production services should be run on Shared Projects only.

Shared projects allows VMs which are being administered by a team to be defined with a specific quota. This quota can be increased if required. Within a project, the administrators of the project are able to start/stop/create/delete any virtual machine in the project.

A user can be on multiple projects at the same time and switch between them using the Dashboard or different environment variables.

**Note:** There is currently no mechanism to move a VM from one project to another, including transferring personal project VMs to shared projects.
There are some resources which can be declared as private for a project. This would mean that private versions or new definitions of these could be made available such as images and flavors.

The following section describes how the administrator of a project can perform standard operations in the CERN cloud  environment.