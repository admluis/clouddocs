# Project roles
A shared project is a set of resources which is available to more than one person (as opposed to the personal projects which are allocated to an individual).
A brief summary of the current characteristics is as follows. There will be further evolutions of this model in future revisions of the service.

| Function | Personal | Shared |
| -- | -- | -- |
|Ownership|Owned by the account|Owned by the person requesting the project|
|Creation|Automatically created when the person subscribes to the private cloud service|Created manually using the process described|
|Deletion|Removed when the person leaves CERN along with all virtual machines in the personal project.|Deletion is manual operation on request|
|Member Role|There are no members other than the person.|Members can be added to the project. These new members can perform the same operations as the owner. The list of members can be defined as an e-group.|
|[Account type](http://account.cern.ch/account/Help/?kbid=011010)|Primary accounts only|Secondary and service accounts can be members. Onwers must be a primary account.|
|[Flavors](http://clouddocs.web.cern.ch/clouddocs/details/flavors.html)|Only the standard m1.* flavors are available|Additional flavors can be requested|
|Quota|Quota is fixed|Additional quota for storage, CPU and images can be requested|
|Operator access|No access for operators or sysadmins|Start/Stop/Console operations available on request via the service desk for a project according to role|

The rights of the roles are as follows

| Role | Description |
| -- | -- |
| Member | VM creation within the quota of the project<br>VM deletion<br>Connect to console<br>Shutdown/Start/Reboot<br>Upload new images<br>Delete images<br><p>**Note:** these operations can be performed on any instance inside the project regardless of which account created the VM initially|
|Operator|Connect to console<br>Shutdown/Start/Reboot<br><p>These rights are given to the computer centre operators to support out of hours interventions and are optional. Shared projects can request Operator access at project creation time.|

**Note:** The VM can be configured to allow users other than members and operators to log into it. These controls are done using the standard Linux mechanisms such as through Puppet. Members should be considered as those administering the VMs rather than logging into them.

## Changing members

The members of a project can be changed for existing projects using the [resources portal](https://resources.web.cern.ch/resources/Manage/CloudInfrastructure/Resources.aspx) in the Cloud Infrastructure section.

## Adding operator and sysadmin access

If you wish VMs in your project to be controlled by the computer centre operations or system administrator teams, this can be enabled on a per-project basis.

This is enabled from the [Resources](https://resources.web.cern.ch/resources/Manage/CloudInfrastructure/Resources.aspx) and enable the *Operators* option.

![Picture showing which box to tick](http://cern.ch/clouddocs/images/opsallow.png)
