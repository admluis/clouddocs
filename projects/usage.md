# Usage

There are a number of commands which can provide details of the resource usage of project. The command line tools have many options but some examples are given here.

**Note:** There are no options for these commands currently using the unified client.

## Compute

Showing the current utilisation and maximum utilisation since project creation.

```
nova absolute-limits
+-------------------------+-------+
| Name                    | Value |
+-------------------------+-------+
| maxServerMeta           | 128   |
| maxPersonality          | 5     |
| maxImageMeta            | 128   |
| maxPersonalitySize      | 10240 |
| maxTotalRAMSize         | 51200 |
| maxSecurityGroupRules   | 0     |
| maxTotalKeypairs        | 100   |
| totalRAMUsed            | 14336 |
| maxSecurityGroups       | 0     |
| totalFloatingIpsUsed    | 0     |
| totalInstancesUsed      | 4     |
| totalSecurityGroupsUsed | 0     |
| maxTotalFloatingIps     | 0     |
| maxTotalInstances       | 10    |
| totalCoresUsed          | 7     |
| maxTotalCores           | 20    |
+-------------------------+-------+```

Showing the consumption of CPU resources during a time window

```
nova usage --start 2014-01-01 --end 2014-01-31 --tenant "841615a3-ece9-4622-9fa0-fdc178ed34f8"

Usage from 2014-01-01 to 2014-01-31:
+-----------+--------------+-----------+---------------+
| Instances | RAM MB-Hours | CPU Hours | Disk GB-Hours |
+-----------+--------------+-----------+---------------+
| 8         | 18784568.32  | 9172.15   | 183443.05     |
+-----------+--------------+-----------+---------------+```

## Volumes

To show current usage and quota for volumes on a project.

```
cinder quota-usage "841615a3-ece9-4622-9fa0-fdc178ed34f8"

+--------------------+--------+----------+-------+
|        Type        | In_use | Reserved | Limit |
+--------------------+--------+----------+-------+
|     gigabytes      |   20   |    0     |  1000 |
| gigabytes_standard |   20   |    0     |   -1  |
|     snapshots      |   0    |    0     |   10  |
| snapshots_standard |   0    |    0     |   -1  |
|      volumes       |   1    |    0     |   10  |
|  volumes_standard  |   1    |    0     |   -1  |
+--------------------+--------+----------+-------+```


