# Set up your lxplus account

Before using the CERN cloud, you must be subscribed to the cloud service. See [Subscribe](http://clouddocs.web.cern.ch/clouddocs/tutorial_using_a_browser/subscribe_to_the_cloud_service.html) for more details.

## Create your shell profile
Logging into lxplus, create a file called .openrc in your home directory with the contents as follows

```
export OS_AUTH_URL=https://openstack.cern.ch:5000/v2.0
export OS_USERNAME=`id -un`
export OS_TENANT_NAME="Personal $OS_USERNAME"

# With Keystone you pass the keystone password.
echo "Please enter your OpenStack Password: "
read -s OS_PASSWORD_INPUT
export OS_PASSWORD=$OS_PASSWORD_INPUT```

Source the profile as part of your shell with

```
$ source .openrc```

See [Environment options](http://clouddocs.web.cern.ch/clouddocs/using_openstack/environment_options.md) for a more in depth explanation of openrc files, including alternative authentication methods.

## <a id=keypair>Create your OpenStack keypair</a>

On lxplus, you will have an ssh key pair in ~/.ssh/id_dsa.pub. If you do not have one, run ```
ssh-keygen -t dsa```
 to generate one. This will be used to ssh into the VMs once they are created.

Now that you have created your keypair in ~/.ssh/id_dsa.pub, you can upload it to OpenStack as follows

```
$ openstack keypair create --public-key ~/.ssh/id_dsa.pub lxplus
+-------------+-------------------------------------------------+
| Field       | Value                                           |
+-------------+-------------------------------------------------+
| fingerprint | 5c:c1:af:06:03:e8:db:68:bb:d6:6a:73:75:82:2a:81 |
| name        | lxplus                                          |
| user_id     | timbell                                         |
+-------------+-------------------------------------------------+
```




