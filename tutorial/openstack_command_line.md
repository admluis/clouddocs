# Create a virtual machine

## Pre-requisities
* Subscribe to cloud service
* Download your openstack profile




## List images

An OpenStack image is used to define the operating system for a virtual machine. The images available can be listed using

```
$ openstack image list
+--------------------------------------+-------------------------------------------+
| ID                                   | Name                                      |
+--------------------------------------+-------------------------------------------+
| 60366d54-63d8-4e58-97cf-9923bb21c538 | CC7 Server - x86_64 [2014-08-12]          |
| d1cb4dce-7a03-4342-a6c1-9677ecb8770d | SLC6 Server - x86_64 [2014-08-05]         |
| e05c34f7-afcc-4c69-985e-6d9c75011723 | SLC6 Server - i686 [2014-08-05]           |
| 13ec4721-3f9b-4480-a29e-ccfd897120d7 | SLC6 CERN Server - x86_64 [2014-08-05]    |

```

The SLC6 CERN Server image can be selected for the next steps in the tutorial.

## Creating your first VM

With the image ID from above, you can create a new VM. Change the hostname (*timdoc143* in the example) to the name of the host you wish to create.

```
$ openstack server create --key-name lxplus --flavor m1.small --image "SLC6 CERN Server - x86_64 [2014-08-05]" timdoc143
+--------------------------------------+--------------------------------------------------------------------------+
| Field                                | Value                                                                    |
+--------------------------------------+--------------------------------------------------------------------------+
| OS-DCF:diskConfig                    | MANUAL                                                                   |
| OS-EXT-AZ:availability_zone          | nova                                                                     |
| OS-EXT-STS:power_state               | 0                                                                        |
| OS-EXT-STS:task_state                | scheduling                                                               |
| OS-EXT-STS:vm_state                  | building                                                                 |
| OS-SRV-USG:launched_at               | None                                                                     |
| OS-SRV-USG:terminated_at             | None                                                                     |
| accessIPv4                           |                                                                          |
| accessIPv6                           |                                                                          |
| addresses                            |                                                                          |
| config_drive                         |                                                                          |
| created                              | 2014-09-28T13:45:56Z                                                     |
| flavor                               | m1.small (2)                                                             |
| hostId                               |                                                                          |
| id                                   | 54a54712-2e65-499f-8f9a-4d0217cb76ba                                     |
| image                                | SLC6 CERN Server - x86_64 [2014-08-05] (d1cdce-7a03-4342-a6c1-9677e770d) |
| key_name                             | lxplus                                                                   |
| name                                 | timdoc143                                                                |
| os-extended-volumes:volumes_attached | []                                                                       |
| progress                             | 0                                                                        |
| properties                           |                                                                          |
| security_groups                      | [{u'name': u'default'}]                                                  |
| status                               | BUILD                                                                    |
| tenant_id                            | 841615a3-ece9-4622-9fa0-fdc178ed34f8                                     |
| updated                              | 2014-09-28T13:45:56Z                                                     |
| user_id                              | timbell                                                                  |
+--------------------------------------+--------------------------------------------------------------------------+```

## Follow the VM creation

Follow the VM creation until the state becomes ```
ACTIVE```
 (and the VM is booting)

```
$ openstack server show timdoc143
+--------------------------------------+--------------------------------------------------------------------------+
| Field                                | Value                                                                    |
+--------------------------------------+--------------------------------------------------------------------------+
| OS-DCF:diskConfig                    | MANUAL                                                                   |
| OS-EXT-AZ:availability_zone          | cern-geneva-b                                                            |
| OS-EXT-STS:power_state               | 1                                                                        |
| OS-EXT-STS:task_state                | None                                                                     |
| OS-EXT-STS:vm_state                  | active                                                                   |
| OS-SRV-USG:launched_at               | 2014-09-28T13:53:19.000000                                               |
| OS-SRV-USG:terminated_at             | None                                                                     |
| accessIPv4                           |                                                                          |
| accessIPv6                           |                                                                          |
| addresses                            | CERN_NETWORK=128.142.135.237                                             |
| config_drive                         |                                                                          |
| created                              | 2014-09-28T13:45:56Z                                                     |
| flavor                               | m1.small (2)                                                             |
| hostId                               | 220027c07c6b06f38f6f2fa010cdd193271e55e9983851fc5d9c5779                 |
| id                                   | 54a54712-2e65-499f-8f9a-4d0217cb76ba                                     |
| image                                | SLC6 CERN Server - x86_64 [2014-08-05] (d1dce-7a03-4342-a6c1-9677e8770d) |
| key_name                             | lxplus                                                                   |
| name                                 | timdoc143                                                                |
| os-extended-volumes:volumes_attached | []                                                                       |
| progress                             | 0                                                                        |
| properties                           |                                                                          |
| security_groups                      | [{u'name': u'default'}]                                                  |
| status                               | ACTIVE                                                                   |
| tenant_id                            | 841615a3-ece9-4622-9fa0-fdc178ed34f8                                     |
| updated                              | 2014-09-28T13:53:19Z                                                     |
| user_id                              | timbell                                                                  |
+--------------------------------------+--------------------------------------------------------------------------+```


## Check console of virtual machine

The console for a Linux VM can be displayed using console log.

```
$ openstack console log show --line 20 timdoc143
```

## Reboot a virtual machine

```
$ openstack server reboot timdoc143```








