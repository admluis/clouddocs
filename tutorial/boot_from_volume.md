# Boot from volume

Virtual machines by default are created on the local disk of the hypervisor. This means that a hypervisor
failure can make the virtual machine unavailable. Depending on the failure, the virtual machine
contents may be lost. For many cloud applications, this is covered by techniques such
as [load balancing](http://clouddocs.web.cern.ch/clouddocs/availability_techniques/load_balancing.html).

One approach to improve the availability in these cases is to boot from a volume. This involves a number of steps

  * Create a volume using an image as the source
  * Create a virtual machine using this volume

## Creating a volume from an image

Identify the image for the initial volume contents from ```openstack image list```. In the example below, this is image id ```1c5607b5-d373-4696-ad37-e2dc764f99be```.

Creating a disk from this image with a size of 80GB is as follows.

```
$ openstack volume create --image  1c5607b5-d373-4696-ad37-e2dc764f99be --size 80 --description "From CC7" timextcc7vol
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| attachments         | []                                   |
| availability_zone   | nova                                 |
| bootable            | false                                |
| created_at          | 2014-12-14T15:07:35.802654           |
| display_description | From CC7                             |
| display_name        | timextcc7vol                         |
| encrypted           | False                                |
| id                  | de6e41e7-1fce-440e-af6d-898f145acc2d |
| image_id            | 1c5607b5-d373-4696-ad37-e2dc764f99be |
| properties          |                                      |
| size                | 80                                   |
| snapshot_id         | None                                 |
| source_volid        | None                                 |
| status              | creating                             |
| type                | standard                             |
+---------------------+--------------------------------------+```

Checking the status again using ```openstack volume show``` will allow the volume creation to be followed.

  * downloading means that the volume contents is being transferred from the image service to the volume service
  * available means the volume can now be used for booting. A set of volume_image meta data is also copied from the image service.

## Creating a VM using this volume

```
$ openstack server create --flavor m1.medium --key-name lxplus --image 1c5607b5-d373-4696-ad37-e2dc764f99be --block-device-mapping vda=de6e41e7-1fce-440e-af6d-898f145acc2d timextboot143
+--------------------------------------+--------------------------------------------------------------------------------+
| Field                                | Value                                                                          |
+--------------------------------------+--------------------------------------------------------------------------------+
| OS-DCF:diskConfig                    | MANUAL                                                                         |
| OS-EXT-AZ:availability_zone          | nova                                                                           |
| OS-EXT-STS:power_state               | 0                                                                              |
| OS-EXT-STS:task_state                | scheduling                                                                     |
| OS-EXT-STS:vm_state                  | building                                                                       |
| OS-SRV-USG:launched_at               | None                                                                           |
| OS-SRV-USG:terminated_at             | None                                                                           |
| accessIPv4                           |                                                                                |
| accessIPv6                           |                                                                                |
| addresses                            |                                                                                |
| config_drive                         |                                                                                |
| created                              | 2014-12-14T15:19:09Z                                                           |
| flavor                               | m1.medium (3)                                                                  |
| hostId                               |                                                                                |
| id                                   | 6ae774f8-6f53-46a5-ae02-37166037a8ed                                           |
| image                                | CC7 Server - x86_64 [2014-10-31] [TEST] (1c5607b5-d373-4696-ad37-e2dc764f99be) |
| key_name                             | None                                                                           |
| name                                 | timextboot143                                                                  |
| os-extended-volumes:volumes_attached | [{u'id': u'de6e41e7-1fce-440e-af6d-898f145acc2d'}]                             |
| progress                             | 0                                                                              |
| properties                           |                                                                                |
| security_groups                      | [{u'name': u'default'}]                                                        |
| status                               | BUILD                                                                          |
| tenant_id                            | 841615a3-ece9-4622-9fa0-fdc178ed34f8                                           |
| updated                              | 2014-12-14T15:19:09Z                                                           |
| user_id                              | timbell                                                                        |
+--------------------------------------+--------------------------------------------------------------------------------+
```

For Scientific Linux 5 and 6, the system disks will need to be manually grown as documented in [Guest specific procedures](http://clouddocs.web.cern.ch/clouddocs/guest_specific_procedures/README.html).
