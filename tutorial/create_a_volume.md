# Create a volume
## Pre-requisities
* Create a virtual machine

## Creating a volume

This allows an arbitrary sized disk to be attached to your virtual machine, like plugging in a USB stick. The steps below create a disk of 20GB.

```
$ openstack volume create --size 20 timvol143
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| attachments         | []                                   |
| availability_zone   | nova                                 |
| bootable            | false                                |
| created_at          | 2014-09-29T09:38:04.837127           |
| display_description | None                                 |
| display_name        | timvol143                            |
| encrypted           | False                                |
| id                  | cf6bc40d-147a-4c47-9131-45b88521594c |
| properties          |                                      |
| size                | 20                                   |
| snapshot_id         | None                                 |
| source_volid        | None                                 |
| status              | creating                             |
| type                | standard                             |
+---------------------+--------------------------------------+
```

The volume can be checked using volume list.

```
$ openstack volume list
+--------------------------------------+----------------+-----------+------+----------+
| ID                                   | Display Name   | Status    | Size | Attached |
+--------------------------------------+----------------+-----------+------+----------+
| cf6bc40d-147a-4c47-9131-45b88521594c | timvol143      | available |   20 |          |
+--------------------------------------+----------------+-----------+------+----------+
```


## Attaching the volume to a VM

Once the status is created, it can be attached to a virtual machine. The following command attaches the volume timvol143 to the virtual machine timslc6143.

```
openstack server add volume timslc6143 timvol143
```

Logging into the VM, this can be seen in /proc/partitions as an additional disk

```
# cat /proc/partitions
major minor  #blocks  name

 252        0   20971520 vda
 252        1     409600 vda1
 252        2   10075136 vda2
 253        0    1572864 dm-0
 253        1    8486912 dm-1
 252       16   20971520 vdb
 ```


and then formatted and mounted.


```
# mkfs -t ext4 /dev/vdb
# mount /dev/vdb /mnt
df -H
Filesystem            Size  Used Avail Use% Mounted on
...
/dev/vdb               22G  181M   20G   1% /mnt
```



