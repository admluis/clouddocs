# Contributing

This section describes different ways that CERN users can contribute to improve
the cloud service.

## Improving the service documentation

The [CERN Cloud User Guide](http://cern.ch/clouddocs) is stored on the CERN Gitlab
facility.

Your comments on this document are welcome! Please open a
[service desk ticket](http://clouddocs.web.cern.ch/clouddocs/overview/getting_help.html)
with details of the URL, and a suggestion to improve it.

If you are an experienced user of tools such as [git](http://cern.ch/git) and
[gitlab](http://cern.ch/gitlab), you can also submit your changes to our public
documentation as a merge request.

This section describes the main steps to do it.

### Fork the repository

The CERN Cloud documentation is hosted on the CERN GitLab service using a
protected repository. The first step to submit your changes is to fork the
repository, resulting in a writable copy of the documentation owned by you.

First, you have to login to the [gitlab](https://gitlab.cern.ch/) application
and access the [cloud-infrastructure/clouddocs](https://gitlab.cern.ch/cloud-infrastructure/clouddocs)
project.

Fork the project using the link on top-right corner of the page, this will allow
you to create your own local copy (for example `my-user-name/clouddocs`).
  
### Write your change

The documentation uses [gitbook](https://github.com/GitbookIO/gitbook). GitBook 
is a tool for building books using Markdown. Markdown is a powerfull and easy-to-use markup language. You can find a quick introduction
[here](https://gitlab.com/gitlab-org/gitlab-ce/blob/6-4-stable/doc/markdown/markdown.md).

Depending on your skills and preferences, you can update the documentation using
the GitLab web interface or the git command-line utilities.

#### Using GitLab

The easiest way to make your changes to the documentation is using the GitLab
web interface. 

  * Login to the [gitlab](https://gitlab.cern.ch/) and access your fork using 
    the links on the right side of the page.
  * Create a _New Branch_ (select 3 bars on right and then git branch option). Enter
    the name of your branch and 'master' for the tag name.
  * From the Files menu, you can see the file structure inside the web browser
  * Make the change needed using the _Edit_ option for the files you want to
    change. This gives a simple edit window in the browser. Save the changes
    with a commit comment.

#### Using the command line (lxplus)

Similar steps can be done on the command line using git. We propose the following
workflow while contributing to the documentation:

  * The first step is to clone the fork you created and create a reference called
    _upstream_ pointing to the main repository:
    
```
$ git clone ssh://git@gitlab.cern.ch:7999/my-user-name/clouddocs.git
$ cd clouddocs
$ git remote add upstream https://gitlab.cern.ch/cloud-infrastructure/clouddocs.git
```

  * Before you start, it is recommended to update your local copy with the new
    changes that may have made it upstream.
    
```
$ git checkout master
$ git fetch upstream
$ git rebase upstream/master
$ git push origin master
```

  * The previous step will give you an updated local master branch from which you
    can start your work:

```
$ git checkout -b my-doc-contribution            # Create the branch for your work
$ vi SUMMARY.md                                  # Do your modifications
$ git add SUMMARY.md                             # Mark the files to be committed
$ git commit -m 'Fix the table of contents'      # Commit your work with a descriptive message
$ git fetch upstream                             # Rebase your changes with latest modifications upstream
$ git rebase upstream/master
$ git push origin my-doc-contribution            # Push the local work to your fork on GitLab.
```

### Submit your change

The last step is to create a merge request to the main repository. The merge
request must be done using GitLab web interface:

  * Login to the [gitlab](https://gitlab.cern.ch/) and access your fork using 
    the links on the right side of the page.
  * Go to the list of branches (using the _branches_ link) and locate your branch.
  * Choose _Compare_ and then _Make a merge request_. This will allow a final
    comment for the merge and submit it to the project maintainers to review and
    approve the change.

The benefits of doing this is that it is much easier to describe the change to
be made and track the inclusion.

## Patching the code

OpenStack is an open source project. At CERN, we use the RDO distribution where an 
open community package the source code from the OpenStack community.

The code for OpenStack is available on [github](https://github.com/openstack). 
To contribute to the code, you can follow the steps outlined by the 
[OpenStack Foundation](https://wiki.openstack.org/wiki/How_To_Contribute).