# Current Restrictions

The following are known restrictions for the service for the users. The aim is to resolve these with the relevant open source community.
   * Security groups for virtual machines under OpenStack are currently not created correctly when using the cells configurations for large cloud configurations. The default rule therefore lets all traffic and the VMs should be configured to using iptables to control access to the TCP/IP ports [launchpad](https://bugs.launchpad.net/nova/+bug/1274325)
   * Server Groups currently cannot be created in the CERN OpenStack environment due to an issue with the [OpenStack cells support](https://bugs.launchpad.net/nova/+bug/1369518)
   * The cloud-init plug in for virtual machines on SLC5 and SLC6 does not automatically resize the root partition to use all the space in the flavor in SLC5 and SLC6 images. A [manual work around](../guest_specific_procedures/README.html) is available.. For CERN CentOS 7, the disk is extended automatically as expected.
   * The volume service is currently not available for Windows VMs. Potential solutions are being investigated to deliver this functionality with expected delivery in Q2 2015.
   * The standard OpenStack openrc mechanism for the nova, glance, cinder and keystome commands sets the user password in an environment variable. This can be read by the superuser on the server when a command is running. Kerberos and X.509 support are available using the unified openstack command line tool. Alternatively, the EC2 keypair approach can be used or [keystone client sessions](http://www.jamielennox.net/blog/2014/09/15/how-to-use-keystoneclient-sessions/). This may be resolved by future upstream changes with the Keystone V3 API support.
   * The [OpenStack client](https://wiki.openstack.org/wiki/OpenStackClient) which supports Kerberos and X.509 authentication as well as passwords has some missing functionality which requires use of the alternative commands (such as nova, cinder or glance). These in turn require currently  password authentication. The missing functionality includes listing availability zones, extending cinder volumes and sharing images. These have been reported upstream and are being tracked in [launchpad](https://bugs.launchpad.net/python-openstackclient/+filebug)
   * Not all OpenStack features have been configured in the CERN cloud. The following is a non-exhaustive list.
      * Instance [shelving and unshelving](http://docs.openstack.org/user-guide/content/shelve_server.html)
      * Volume backup using cinder-backup (openstack backup create command)
      * Floating IPs
      * OpenStack services other than Nova, Cinder, Glance, Keystone and Horizon are not configured.
   * Pie charts in the OpenStack web interface do not display correctly on Internet Explorer version 9 and 10. Internet Explorer 11 displays correctly. This is unlikely to be resolved.
   * The EC2 interface compatibility has several restrictions or incomplete implementations. The reported ones are
      * InstanceInstantiatedShutdownBehavior is not implemented
      * Tags are not implemented
      * Rate limiting is not applied, thus an EC2 client is able to create significant load on the cloud controllers

## Upcoming Enhancements

The following are being planned as part of the incremental enhancements to the service in the upcoming months. The order is in the expected order of delivery.

   * Detailed accounting reports of usage by project for the project owners and computing co-ordinators  (estimated Q2 2015)
   * Grouping of projects into a tree like hierachy. This will allow, for example, all CMS related projects to be included into a single domain with a top level quota. The exact function is still under development within the OpenStack community (estimated Q2 2015)
   * [OpenStack Heat](http://docs.openstack.org/user-guide/content/heat-stack-create.html) project for orchestration providing scale-out and VM restart (estimated Q2 2015)
