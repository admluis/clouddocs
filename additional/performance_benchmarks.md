# Performance benchmarks
This section defines the process for running the performance benchmarks used in this documentation.

## Disk I/O with bonnie++

[Bonnie++](http://www.coker.com.au/bonnie++/) provides a simple read/write disk I/O performance benchmark.

```
yum install -y bonnie++
bonnie++ -d /tmp -u root```

This test runs for about 10 minutes.

```
Version  1.96       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
timperf01.cer 7656M   294  99 68834  15 38694   9  1458  96 114325  10 398.0  18
Latency             30575us     883ms    2524ms   29026us   95506us   90168us
Version  1.96       ------Sequential Create------ --------Random Create--------
timperf01.cern.ch   -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 10082  74 +++++ +++ 28534  61 11125  81 +++++ +++ 26378  57
Latency               662us     645us     626us     628us     190us     177us
```
Write I/O is thus around 68MB/s sequential (from 68834K/sec sequential output), read I/O at around 110MB/s. IOPS corresponds to the Seeks/s value (398).

See the [performance tuning chapter](http://clouddocs.web.cern.ch/clouddocs/advanced_topics/performance_tuning.html) for the options to improve I/O

## Memory bandwidth

The reference benchmark used for memory performance is the stream benchmark.

```
yum -y install gcc wget
wget http://www.cs.virginia.edu/stream/FTP/Code/Makefile
wget http://www.cs.virginia.edu/stream/FTP/Code/stream.c
wget http://www.cs.virginia.edu/stream/FTP/Code/mysecond.c
make stream_c.exe
./stream_c.exe```

Output such as below is produced

```
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:            8503.4     0.020086     0.018816     0.020925
Scale:           8342.8     0.020838     0.019178     0.021833
Add:             8644.6     0.028610     0.027763     0.029654
Triad:           8739.0     0.029437     0.027463     0.031352
-------------------------------------------------------------```

The performance figure for the service level is based on the Triad figure (i.e. 10927.5 MB/s)


