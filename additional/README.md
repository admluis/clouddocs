# Additional Information

The CERN cloud is based on OpenStack. While this user guide is specific to CERN, there are other sources of generic information about OpenStack on the Internet. The following contains some pointers for operations outside the scope of this guide or which are related to the OpenStack architecture and design.

##<a id=openstack>General Information</a>

|Description|Link|
|-----------|----|
|OpenStack project home page|http://openstack.org|
|OpenStack documentation|http://docs.openstack.org|
|OpenStack end user guide|http://docs.openstack.org/user-guide/|
|OpenStack administration guide|http://docs.openstack.org/ops/|

##<a id=cerncloud>CERN OpenStack Information</a>

|Description|Link|
|-----------|----|
|CERN Cloud Blog|http://openstack-in-production.blogspot.com/|
|OpenStack 101 : Introduction to CERN cloud for users|[Video](http://indico.cern.ch/event/300401/contribution/0/material/0/) <p>[Slides](http://indico.cern.ch/event/300401/contribution/0/material/slides/0.pdf)|
|CERN Cloud Training Material|[Material](http://information-technology.web.cern.ch/sites/information-technology.web.cern.ch/files/OpenStack_Training_June_2014.pdf)|
|HEPiX updates|[Slides Spring 2014](https://indico.cern.ch/event/346931/session/9/contribution/16/material/slides/0.pdf) <p>[Slides 2013](http://indico.cern.ch/getFile.py/access?contribId=11&sessionId=4&resId=0&materialId=slides&confId=247864)|
|Heat at CERN|[Slides](https://indico.cern.ch/event/346931/session/9/contribution/22/material/slides/0.pdf)|
|CERN Cloud Deep Dive from Hong Kong Summit in 2013|[Talk](http://www.openstack.org/summit/openstack-summit-hong-kong-2013/session-videos/presentation/deep-dive-into-the-cern-cloud-infrastructure)|
|CHEP 2013 Presentation|[Slides](http://indico.cern.ch/getFile.py/access?contribId=217&sessionId=8&resId=0&materialId=slides&confId=214784)|
|CHEP 2012|[Paper](http://cern.ch/go/FwG7)|

##<a id=cernproject>OpenStack CERN Project Information</a>

These presentations describe the work in the OpenStack community on projects we are working on with the OpenStack community and other labs.

|Description|Link|
|-----------|----|
|OpenStack Federation (developed as part of Rackspace Openlab collaboration)|[Video](https://www.openstack.org/summit/openstack-paris-summit-2014/session-videos/presentation/cloud-federation-are-we-there-yet) <p>[Slides](http://www.slideshare.net/noggin143/openstack-paris-federation?qid=f61da243-440e-4a70-9097-d4f2bd72e74e&v=default&b=&from_search=2)|
|
|OpenStack Federation (technical aspects)|[Link](https://www.openstack.org/assets/presentation-media/os-federation-final.pdf)|
|OpenStack Federation Setup Instructions|[Documentation](http://docs.openstack.org/developer/keystone/extensions/federation.html)|

## CEPH at CERN

|Description|Link|
|-----------|----|
|CEPH usage at CERN | [March 2014](http://www.slideshare.net/Inktank_Ceph/scaling-ceph-at-cern) |


## Other Academic Clouds

|Description|Link|
|-----------|----|
|CMS Trigger Farm OpenStack cloud|[Paper](http://iopscience.iop.org/1742-6596/513/3/032019/pdf/1742-6596_513_3_032019.pdf)|
|ALICE Trigger Farm OpenStack cloud|[Slides](https://indico.cern.ch/event/324708/session/4/contribution/67/material/slides/0.pdf)|
|ATLAS Trigger Farm OpenStack cloud|[Paper](http://iopscience.iop.org/1742-6596/513/6/062037/pdf/1742-6596_513_6_062037.pdf)|