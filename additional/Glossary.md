Glossary
========

There are many specialised terms used within a cloud. This page lists some of them and their meaning in the cloud environment at CERN. A full glossary can be found as part of the [OpenStack documentation](http://docs.openstack.org).

|Term|Meaning|
|----|-------|
|Availability Zone|A set of hypervisors which may share single points of failure. Services can schedule VMs to different availability zones in order to  Caavoid a single failure causing application down time. See higher availability for more information.|
|Cattle|Virtual machines which can be running on low service levels such as redundant web servers without any data stored locally. Cloud computing models tend towards using Cattle with reliable data stores rather than Pets (see later) such that new Cattle can be created and deleted/fail as needed according to the workload.|
|Ceilometer|The [OpenStack metering system](https://wiki.openstack.org/wiki/Ceilometer) which keeps track of usage of virtual machines, storage and provides accounting data.|
|Cell|A unit of an OpenStack deployment which consists of up to 1000 hypervisors.|
|Ceph|[Ceph](http://ceph.com) is an open source project which provides storage services. For OpenStack, this is used to support the block storage behind Cinder and the image storage behund Glance. More details of Ceph at CERN can be found at http://cern.ch/ceph |
|Cinder|The Openstack block storage system which allows additional disk volumes to be attached to virtual machines.|
|Cloud Computing|Cloud computing is a computing model, where resources such as computing power, storage, network and software are abstracted and provided as services on the Internet in a remotely accessible fashion. Billing models for these services are generally similar to the ones adopted for public utilities. On-demand availability, ease of provisioning, dynamic and virtually infinite scalability are some of the key attributes of cloud computing.|
|Contextualisation| The process of passing custom paramters to a virtual machine when it is created. This is often done using the cloud-init package.|
|Core|A processing unit of a CPU|
|EC2|Amazon's public cloud is the most commonly used public cloud and established a de-facto API for talking to clouds. More details at http://en.wikipedia.org/wiki/Amazon_Elastic_Compute_Cloud|
|Ephemeral|Ephemeral storage is associated with a single unique instance. Its size is defined by the flavor of the instance. Data on ephemeral storage ceases to exist when the instance it is associated is deleted.|
|Flavor|	The description of the (virtual!) hardware of size of a virtual machine such as 1 virtual CPU, 40 GB of disk space and 4 GB memory. More details on Flavors here.|
|Floating IP|OpenStack uses the term "floating IP" to refer to an IP address (typically public) that can be dynamically added to a running virtual instance.<p>**Note**: Floating IPs are currently not supported on the CERN Private Cloud as the IP addresses are public by default.|
|Glance|The component of OpenStack which stores the reference images|
|Grizzly|The OpenStack release which was released in April 2013 and made available at CERN in July 2013.|
|Havana|An OpenStack release first made available in Autumn 2013 and in production at CERN during Q1 2014|
|Heat|The OpenStack Orchestration service|
|Hypervisor|Software that arbitrates and controls VM access to the actual underlying hardware|
|Horizon|The web based dashboard of OpenStack providing a self-service portal for end users to manage their virtual machines and projects.|
|Ibex|A pre-production instance of OpenStack which was used at CERN from February 2012 to July 2013. This was based on the Folsom release of OpenStack.|
|Icehouse|The OpenStack release made available in Q3 2014.|
|Image|A reference copy of an operating system with optional additional software which can be used to create virtual machines. Typical examples would be a Linux Server or Windows 7 image.|
|Juno|The OpenStack release expected to be written during Autumn 2014|
|Keypair|A secret key which is used to access private resources. This should be kept safe in a private directory with limited access and not shared.|
|Keystone|The component of OpenStack which checks users' identity (via passwords or certificates) and their roles (such as project manager or user)|
|Nova|The compute controller in OpenStack which manages the hypervisors and co-ordinates the operations from the dashboard.|
|OpenStack|   OpenStack is a collection of open source software projects that enterprises/service providers can use to setup and run their cloud compute and storage infrastructure. This is the software in use for the CERN private cloud. Details are available at http://openstack.org.
|Orchestration|	A cloud technology which allows creation of multiple VM application templates. This is implemented in OpenStack using the Heat service
|Pets|	Servers which need to be carefully looked after and are not stateless. Cloud computing models encourage the use of Cattle servers but this is not always feasible for all applications. Running 'Pet' servers requires a much higher effort and more complex infrastructure compared to 'Cattle'.
|Project|	An activity with a set of related virtual machines and a quota for maximum capacity.
|Snapshot|    A copy of a disk image which may be possible to use in the event of problems or to make a volume with the same data. Great care needs to be taken to ensure consistency of the snapshot.
|Security Group|	A list of network communication rules for connecting to and from a set of virtual machines. Security groups are independent of the firewall configuration on the machine or at the site level.
|Tenant|	A legacy term which has now been replaced by Project.
|Virtual Machine|	A virtual machine (VM) is a software implementation of a machine (i.e. a computer) that executes programs like a physical machine.
|VNC| A way to access a machine remotely. VNC is used in the private cloud to get access to the VM console through the dashboard.
|Volume|	A virtual disk drive which can be attached to a virtual machine to provide additional storage space.