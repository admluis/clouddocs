# References

The following set of articles, blogs and videos have been published to describe the CERN Cloud project and its related monitoring and confiugration management activities.

It is organised into two sections

  * Summary for main interest
  * Set of sub-topics including historical papers

The documentation for OpenStack itself can be found at http://docs.openstack.org. Detailed guides are described in [Getting Help](http://clouddocs.web.cern.ch/clouddocs/overview/getting_help.html)

## Summary

| Description | Link | Date |
| -- | -- | -- |
| OpenStack In Production description of hints and tips of the CERN cloud | [Blog](http://openstack-in-production.blogspot.fr/) | 2012 onwards |
| CERN OpenStack Tutorial | [Slides](http://indico.cern.ch/event/300401/contribution/0/material/0/), [Video](http://cds.cern.ch/record/1692588) | 2012 |
| CERN OpenStack Introduction | [Slides](http://www.slideshare.net/noggin143/20141103-cern-openstackparisv3?qid=f61da243-440e-4a70-9097-d4f2bd72e74e&v=default&b=&from_search=9) | 2014 |
| CERN Technical Deep Dive | [Slides](https://www.openstack.org/assets/presentation-media/Deep-Dive-into-the-CERN-Cloud-Infrastructure.pdf) [Video](http://www.youtube.com/watch?v=QJll5nBclh4&feature=youtube_gdata) | 2013 |

## Papers

| Description | Link | Date |
| -- | -- | -- |
| Experiment commisioning of Agile Infrastructure (CHEP) | [Paper](http://cds.cern.ch/record/1622187/files/CERN-IT-2013-003.docx?version=1) | 2013 |
| The Agile Infrastructure project | [Paper](http://cern.ch/go/FwG7) | 2012 |

## Federation

These documents describe the OpenStack Federated Clouds activities including work by CERN [Openlab](http://cern.ch/openlab) in collaboration with Rackspace.

| Description | Link | Date |
| -- | -- | -- |
| Federation - Are we there yet ? | [Video](https://www.openstack.org/summit/openstack-paris-summit-2014/session-videos/presentation/cloud-federation-are-we-there-yet) [Slides](http://www.slideshare.net/noggin143/openstack-paris-federation?qid=f61da243-440e-4a70-9097-d4f2bd72e74e&v=default&b=&from_search=2) | 2014 Paris Summit |
| Federation in OpenStack technical presentation | [Slides](https://www.openstack.org/assets/presentation-media/os-federation-final.pdf) | 2014 Atlanta |
| Enabling the Federation Extension | [Documentation](http://docs.openstack.org/developer/keystone/configure_federation.html) | 2014 |
| Toward Hybrid Clouds | [Slides](http://fr.slideshare.net/andrewrhickey/cern-rackspaceopen-stacksummit)| 2013 Hong Kong (original proposal) |

## Quota
| Description | Link | Date |
| Quota manager proposal | [Paper](http://indico.cern.ch/event/253853/material/0/0.pdf) | 2013 |

## Ceph Storage

| Description | Link | Date |
| -- | -- | -- |
| Ceph at CERN | [Slides](http://www.slideshare.net/Inktank_Ceph/scaling-ceph-at-cern?qid=f61da243-440e-4a70-9097-d4f2bd72e74e&v=qf1&b=&from_search=16) | 2014 |

