# Releases

The CERN cloud has been in production since July 2013. The releases follow the OpenStack ones during the six months after the release.

## <a id=juno>Juno</a>

The Juno release was installed in April 2015 at CERN. The details of the release are described in the [Release Notes](https://wiki.openstack.org/wiki/ReleaseNotes/Juno) although not all features may be enabled on the CERN cloud.

| Component| Function |
| -- | -- |
| Compute (Nova) | The image to use for rescue can be specified at rescue time |
||Numa support for improved performance on complex CPU configurations|
||Additional LanDB fields can be set for the operating system and version |
| Volumes (Cinder) | Copy on write for faster cloning of external volumes |
|| Support for volume replication |
| Dashboard (Horizon) | Improved user interface |
| Identity (Keystone) | |

## <a id=icehouse>Icehouse</a>

The CERN OpenStack cloud was upgraded to the Icehouse release during October 2014. Along with [many bug fixes and additional function for the cloud administrators](https://wiki.openstack.org/wiki/ReleaseNotes/Icehouse), the new user features since the Havana release are below

| Component| Function |
| -- | -- |
| Compute Service (Nova) | VMs can now be created with dual stack IPv4 and IPv6. See Enabling IPv6 for details.|
||The data centre can be selected for projects that span multiple centres. See [meta options](http://clouddocs.web.cern.ch/clouddocs/using_openstack/properties.html).|
||Access to Windows virtual machine consoles is available through the web GUI using RDP|
||Watchdog support is available to reboot machines that become unresponsive|
|Image Service (Glance)|CERN CentOS 7 images are available as standard public images|
|Dashboard (Horizon)|New look and feel based on structured menus|
||Support for Single Sign On using CERN credentials|
||Additional options such as extend volume available in the GUI along with improved wizards for multi-step operations|
|Identity (Keystone)|Support for login using Kerberos and X.509 authentication using the unified command line tool (see below)|
||Projects can now request access for the operators and computer centre sys admin team for operations such as reboot or view console|
||Management of administrators for a project is now self service in the [resource portal](https://resources.web.cern.ch/resources/Manage/CloudInfrastructure/Resources.aspx) rather than via a helpdesk ticket|
|Clients|Unified [openstack command line tool](http://docs.openstack.org/cli-reference/content/openstackclient_commands.html) now available on lxplus which provides a single command to consistently access all the servers.|
|Facilities|Projects can now be requested for resources in the critical area of the CERN computer centre which has redundant power infrastructure. These are GPN only.|

The full release notes are [here](https://wiki.openstack.org/wiki/ReleaseNotes/Icehouse) (not all features may be implemented at CERN).

## Havana

The CERN OpenStack cloud upgrade to Havana was completed on 26th February 2014.

| Commponent | Function |
| -- | -- |
|Compute Service (Nova)|End user can modify LanDB network data (link)|
||User can select quick VM creation but to leave the host name consistent with its default random name (cern-update-hostname)|
||Start / End date for a VM is now available for query|
|Image Service (Glance)|Image properties can be set using glance image-update --property|
||Images can be references to other web locations (such as on an external web server) using --location|
||Images can be shared between projects (link)|
|Volume Service (Cinder)|Moved to production status providing the ability to request arbitrary sizes of disk volumes to be attached to virtual machines (link)|
||Boot from volume is now supported allowing varying sizes of system disk|
||Resize of a volume is now supported (link)|
|Dashboard (Horizon)|Operations now available through the web interface (previously CLI only)|
||Availability zones|
||Lots of extra VM operations (suspend/resume/…)|
||Boot from volume|
||Resize VM|
||Different presentation for quota usage|

The full release notes for the OpenStack software are [here](https://wiki.openstack.org/wiki/ReleaseNotes/Havana) (some features may not be available in the CERN cloud)

## Timelines

The following table documents the major milestones in the CERN OpenStack deployment.

| When | Description |
| -- | -- |
| Jan 2011| First presentation on OpenStack to an IT group ([link](https://indico.cern.ch/event/118726/material/slides/1.pdf)) |
| Oct 2011 | First presentation to the Essex OpenStack summit on our tests with Cactus ([link](http://www.slideshare.net/noggin143/cern-user-story))|
| Feb 2013 | First clouds in the experiment online farms started with CMS followed by ATLAS. ALICE started in summer 2014 |
| July 2013 | First production CERN OpenStack release based on Grizzly |
| October 2014 | Migration to Icehouse completed |
| April 2015 | Migration to Juno completed |
