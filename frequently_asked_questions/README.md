# Frequently asked questions

## General

### Not keeping time in guests

The guest VMs will occasionally time clocks will occasionally drift. To ensure time is kept in sync, the CERN time server can be used. For Puppet managed machines, NTP is configured by default.

For Linux hosts which are manually managed, you can use ntpd and ntpdate to synchronise the host.

```
yum install -y ntp
ntpdate ip-time-1
```

## Linux

### Graphical interface on Linux VMs

Recipes to install a graphical interface on [CC7](http://clouddocs.web.cern.ch/clouddocs/guest_specific_procedures/cern_centos_7.html) and [SLC6](http://clouddocs.web.cern.ch/clouddocs/guest_specific_procedures/scientific_linux_cern_6.html) Virtual Machines.

