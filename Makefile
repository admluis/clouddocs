addall:	always
	find  . -print | grep -v "\.git" | xargs -n1 git add

rebase:	always
	git pull origin master

build:
	git commit 
	git push origin master

publish:
	cd /afs/cern.ch/project/clouddocs; make

#       modified:   additional/releases.md
modified:	always
	git status | grep -v _book | awk '/^#	(modified|new file):/ { print $$NF } /Untracked files/ { inuntracked=1 } NF==2 && $$1=="#" && inuntracked==1 { print $$NF }' | grep -v "[.]sw[op]$$" | grep -v "[.]__" | xargs -n1  echo git add

#images:	always
#	cp -pf images/* /afs/cern.ch/project/clouddocs/www/images

always:
