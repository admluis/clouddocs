# Installing your own OpenStack

If you would like to explore OpenStack in depth, you can install your own instance of OpenStack on a virtual machine. The details below use RDO, a community distribution of OpenStack.

## CERN CentOS 7 & Current OpenStack Release (Juno)

If you are using a newly created VM, reboot the VM at least once to get the FQDN as the hostname (see the following [bug](https://bugzilla.redhat.com/show_bug.cgi?id=1134404)).

Then, apply the following steps:

```
yum update -y
yum install -y https://rdo.fedorapeople.org/rdo-release.rpm
sed -i -e '/priority=.*/d' /etc/yum.repos.d/rdo-release.repo
echo "priority=1" >> /etc/yum.repos.d/rdo-release.repo
ssh-keygen -q -f /root/.ssh/id_rsa -t rsa -P ""
cat ~/.ssh/id_rsa.pub >>~/.ssh/authorized_keys
yum install -y openstack-packstack
sed -i -e "4i pidfilepath  => '/var/run/mongodb/mongod.pid'," /usr/lib/python2.7/site-packages/packstack/puppet/templates/mongodb.pp
packstack --allinone --os-neutron-install=n
```

In /usr/lib/python2.7/site-packages/nova/network/linux_net.py you'll need to change a line:

```
<         if interface:
---
>         if interface and interface != 'lo':
```

Otherwise you will get something like

```
NovaException: Failed to add interface: can't add lo to bridge br100: Invalid argument
```

in nova-compute.log. Once done, restart OpenStack:

```
openstack-service restart
```

After creating a keypair:

```
nova keypair-add mykey >> mykey.rsa
chmod 600 mykey.rsa
```

you should be able to launch a first VM using the predfined cirros image:

```
nova boot --image cirros --flavor m1.tiny --key-name mykey cirros01
```

Once that VM is active, you can login with

```
ssh -i mykey.rsa cirros@IPADDRESSOFCIRROS01
```

## CERN CentOS 7 & Previous OpenStack Release (Icehouse) 

If you'd like to use the Icehouse Release of OpenStack, the instructions given
for the Juno release apply as well. The only change needed is that you'll need
to run

```
yum install -y http://rdo.fedorapeople.org/openstack-icehouse/rdo-release-icehouse.rpm
```

instead of

```
yum install -y https://rdo.fedorapeople.org/rdo-release.rpm
```

