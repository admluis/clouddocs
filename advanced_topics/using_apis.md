# Using Cloud Programming Interfaces

There are various programming APIs and language bindings are available for use with CERN private cloud. The support for these comes from the Internet communities rather than the CERN service desk. Different APIs have different levels of portability and compatibility which are included in the Notes section.
General information about using APIs with OpenStack is available in the [OpenStack API documentation](http://api.openstack.org/). Software Development Kits are listed in the [SDK documentation](https://wiki.openstack.org/wiki/SDKs). SDKs which work with OpenStack should work with the CERN private cloud unmodified.

## Language Bindings

These are programming language specific APIs which can be used to access the CERN private cloud. These are suggestions of potential APIs to use
rather than recommendations and the list is not exhaustive.

**Note:** Support for these APIs should be from the Internet communities, not the CERN Service desk

|Programming API|Language|Links|Notes|
| -- | -- | -- | -- |
|Nova OpenStack API using python-novaclient|Python|[Documentation](http://docs.openstack.org/developer/python-novaclient/)<br>[Support](http://lists.openstack.org/pipermail/openstack-dev/)|API provided by the OpenStack foundation. It is OpenStack specific but supports all of OpenStack's functions.  This library is tested with the appropriate OpenStack release as part of OpenStack's standard release testing.|
|libcloud|Python|[Web Site](http://libcloud.apache.org/)<br>[Support](http://mail-archives.apache.org/mod_mbox/libcloud-users/)|Provides different cloud support including OpenStack. This can be useful for the same program talking to different cloud endpoints.|
|jclouds|Java|[Web site](http://www.jclouds.org/)|The standard cloud API used with Java.|
|jstack|JavaScript|[Web site](http://ging.github.com/jstack/)|A JavaScript implementation of the OpenStack API.
|Boto|Python|[Web Site](http://boto.readthedocs.org/en/latest/)|Boto uses the EC2 API. There have been reports of incompatibilities between Boto and OpenStack for some functions.|
|Fog|Ruby|[Web Site](http://fog.io/)|A generic cloud interface library for Ruby programmers. Functionality by cloud endpoint is available [here](http://fog.io/about/supported_services.html)|
|Rubygem-openstack|Ruby|[Web Site](https://github.com/ruby-openstack/ruby-openstack)|Initial tests against CERN private cloud seem to work.|
|OpenStack.NET|.NET|[Web Site](https://github.com/rackspace/openstack.net)|Support for Microsoft's .NET platform|
|php-openstack|PHP|[php-openstack Web Site](https://github.com/rackspace/php-opencloud)| |
|OpenStack-SDK-PHP|PHP|[Web Site](https://wiki.openstack.org/wiki/OpenStack-SDK-PHP)| |
|Net::OpenStack::Compute|Perl|[Web Site](https://github.com/ironcamel/Net-OpenStack-Compute)| |
 
