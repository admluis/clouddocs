# Using the dashboard

The Horizon web GUI provides a simple interface for performing many of the common actions using an interactive display.

All the functions available through Horizon are also available through the command line interfaces.

Some actions which are available on the command line are not available in Horizon, though new releases often add further functions.

