# Managing VMs
## Resizing a VM

**Note:** Resizing a VM will make it unavailable for a short period

Select the instances menu and the resize instance option on the actions.

After the new flavor has been selected (and OK), the status will become resize or migrating.

The status will change after several minutes to Confirm or Revert Resize/Migrate.
You may need to refresh the web browser page to reflect the new status.

Select Confirm Resize/Migrate if you wish to change the instance to the new configuration.

The status will then change to Active once completed.
