CERN OpenStack Private Cloud Guide
==================================

This guide provides details of how to use the CERN cloud infrastructure resources. The CERN Private Cloud provides an Infrastructure-as-a-Service solution integrated with CERN's computing facilities. Using self service portals or cloud interfaces, users can rapidly request virtual machines for production, test and development purposes. The machines can be of different capacities and run a variety of Windows or Linux operating systems.

Am I in the right place ?
-------------------------

CERN IT's policy is for all server machines to be virtualised and hosted on the CERN OpenStack Infrastructure (with a few exceptions). There are two options for making a virtual machine on this infrastructure:

  * **Create a self-managed VM**: the machine you create will be managed by you, or something you setup to do the job, possibly using a special VM image. This is used where the configuration is handled by the end user such as a personal desktop, externnally managed VMs such as Java application server  or a hand configured server. In that case: you are in the right place - read on!

  * **Create a Puppet-managed VM**: the machine you create will be configured using Puppet. This is recommended for longer-lived boxes that provide a service to users. We make reference to the [CERN Configuration Guide](http://cern.ch/config), since the Puppet nodes are hosted on that infrastructure, but, crucially, the tools you use to create and manage the node are different, since they also need to interact with the Configuration Service infrastructure.

## How to use this guide ?

To get productive quickly, you can use the [short of it](overview/README.html) chapter which covers briefly what a cloud provides, concepts and how to use it.

There are several tutorials takes you through creating virtual machines using the different interfaces, the [Horizon web based GUI](http://clouddocs.web.cern.ch/clouddocs/tutorial_using_a_browser/README.html), the [OpenStack command line tools](http://clouddocs.web.cern.ch/clouddocs/tutorial/README.html) and the Amazon EC2 compatible ones.

Further chapters cover the more sophisticated uses of the CERN cloud.

Unless otherwise stated, the commands given should be run from an environment such as lxplus or one of the administration servers such as aiadm.

## What this guide will not do ?

This guide does not cover cloud computing techniques or full details of all of the options of OpenStack. For this, please see [Additional Information](additional/README.html).

For assistance, please use the [service desk portal](https://cern.service-now.com/service-portal) to report problems. Additional pointers are available at [getting help](http://clouddocs.web.cern.ch/clouddocs/overview/getting_help.html).
