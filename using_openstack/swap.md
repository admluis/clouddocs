# Swap

In general, the virtual machine requested should be provided with enough memory to support the application running. The guide below covers cases where swap space is needed for particular purposes.

## Using LVM

Using LVM, the size of the swap space can be changed. The recipe to follow is similar to the one when resizing the root disk, with an intermediate step to grow (in the example below to 8GB) and set up the logical volume for the swap space:

```
growpart /dev/vda 2
```

The VM should then be rebooted. Once rebooted,

```
pvresize /dev/vda2

swapoff -v /dev/VolGroup00/LogVol01
lvresize /dev/VolGroup00/LogVol01 -L 8G
mkswap /dev/VolGroup00/LogVol01
swapon -v /dev/VolGroup00/LogVol01

lvextend -l +100%FREE /dev/mapper/VolGroup00-LogVol00
resize2fs /dev/mapper/VolGroup00-LogVol00
```

## Swap defined in the flavor

Some flavors have a defined swap space. This can be used for a dedicated volume for swap only rather than defining it as part of the image.
For example, this image has 2GB of swap space defined.

```
+----+---------------+-----------+------+-----------+------+-------+-------------+-----------+-------------+
| ID | Name          | Memory_MB | Disk | Ephemeral | Swap | VCPUs | RXTX_Factor | Is_Public | extra_specs |
+----+---------------+-----------+------+-----------+------+-------+-------------+-----------+-------------+
| 86 | vobox.swap    | 2048      | 15   | 20        | 2048 | 1     | 1.0         | True      | {}          |
```

When a VM is created using this flavor which includes cloud-init, the swap space is automatically defined and configured


```
$ swapon -s
Filename                                Type            Size    Used    Priority
/dev/dm-0                               partition       1572856 0       -1
/dev/vdc                                partition       2097144 0       -2
```

