# Resizing a VM

If the size of a virtual machine needs to be changed, such as adding more memory or cores, this can be done using the resize operations. Using resize, you can select a new flavor for your virtual machine and instruct the cloud to adjust the configuration to match the new size.
The operation will reboot the virtual machine and take several minutes of downtime. Network configuration will be maintained but connectivity lost during the reboot so this operation should be scheduled as it will lead to application downtime.

## Graphical Interface using Horizon

Select the instances menu and the resize instance option on the actions.
After the new flavor has been selected (and OK), the status will become resize or migrating.
The status will change after several minutes to Confirm or Revert Resize/Migrate. You may need to refresh the web browser page to reflect the new status.
Select Confirm Resize/Migrate if you wish to change the instance to the new configuration.
The status will then change to Active once completed.
Command Line Interface using Nova

Before resizing,
* Check the current size of the VM using nova show. This is shown on the line marked 'flavor'
* Select a flavor for the new size using nova flavor-list.

To execute the resize operation:
* Run ```nova resize my_server m1.large```
* Check the state of the virtual machine using nova show until it is in the status VERIFY_RESIZE. During the operation, the status line will display RESIZE.
* Log in to the VM and confirm that the resize operations have worked. It may take a minute before ssh can connect to the VM. The ```nova console-log``` command can be used to see when sshd has started again.
* Run the following to accept the change and obtain the status ACTIVE: ```nova resize-confirm my_server```
* To roll back, use ```nova resize-revert```.
* If the disk is a larger size in the new flavor, the following chapter explains how to resize the disk.

