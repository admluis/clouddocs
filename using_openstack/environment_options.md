# Environment options
OpenStack clients find the services to use using environment variables. These are generally stored in a shell file called 'openrc'.

The variables will be different according to the project you are using.

The details of these environment variables can be found in the [OpenStack documentation](http://docs.openstack.org/user-guide/content/cli_openrc.html).

## Download from the dashboard

 Logging into the [OpenStack dashboard](http://cern.ch/openstack), you can download an openrc file as follows.

1. Selecting "Access and Security" from the Compute top menu
2. Choose the API Access Tab
3. Select the Download OpenStack RC File button

This will download a .sh file through your browser that can be used for authentication.

## Local Password script

You can create your own openrc using the following contents. These may vary in future so the openrc may need to be updated. The script below sets the project to your Personal project but the ```
OS_TENANT_NAME```
 variable can be changed as needed.

**Note:** After sourcing this script, your password will be available in the environment of your shell. Depending on the computer on which you are running, this may allow the administrator to access your password. Thus, a Kerberos approach is recommended on public computers.

```
export OS_AUTH_URL=https://openstack.cern.ch:5000/v2.0
export OS_USERNAME=`id -un`
export OS_TENANT_NAME="Personal $OS_USERNAME"

# With Keystone you pass the keystone password.
echo "Please enter your OpenStack Password: "
read -s OS_PASSWORD_INPUT
export OS_PASSWORD=$OS_PASSWORD_INPUT
```

## Kerberos authentication

Where a valid kerberos environment has been configured, this can be used for authentication. The benefit is that there is no password stored in an environment variable.

CERN public services such as lxplus.cern.ch provide this environment.

```
export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
export OS_AUTH_TYPE=v3kerberos
export OS_USERNAME=`id -un`
export OS_TENANT_NAME="Personal $OS_USERNAME"
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
```

If you have a valid kerberos identity (as shown by klist), OpenStack can then be accessed using the unified CLI with these credentials.

Note: This is only available with the ```openstack``` unified CLI currently and not with commands such as nova,cinder,glance etc.

## X.509 authentication

Using a certificate, such as a WLCG grid certificate, you can also access the CERN OpenStack cloud without using a password.

At CERN, certificates can be obtained from the [CERN certification authority](https://gridca.cern.ch/gridca/).

This script assumes your private key and certificate are bundled and located in $HOME/private/osclient.pem.

```
export OS_AUTH_URL=https://keystone.cern.ch:8443/x509/v3
export OS_AUTH_TYPE=v3x509
export OS_IDENTITY_API_VERSION=3
export OS_USER_DOMAIN_ID=default
export OS_CLIENT_CERT=$HOME/private/osclient.pem
export OS_USERNAME=`id -un`
export OS_TENANT_NAME="Personal $OS_USERNAME"
export OS_PROJECT_DOMAIN_ID=default
```

Note: This is only available with the ```openstack``` unified CLI currently and not with commands such as nova, cinder, glance etc.

If you would like to generate new client certificates and private keys, you can perform the following steps using the [CERN CA](https://gridca.cern.ch/gridca/). This follows the steps in the [help](https://gridca.cern.ch/gridca/Help/?kbid=024010) article.














