# Using OpenStack
This chapter documents the command line tools available for interacting with the CERN cloud. These tools are intended for the more experienced users, especially on a Linux platform. For occasional use, the dashboard approach is simpler as documented in [Tutorial using a browser](http://clouddocs.web.cern.ch/clouddocs/tutorial_using_a_browser/README.html).

There are three primary sets of tools available from the command line on a Linux system.

* The unified OpenStack client (openstack). Most of the examples given in this book are based on this command.
* OpenStack tools (nova, glance) which provide the lowest level access to the OpenStack interfaces.
* [Eucalyptus EC2 tools](https://www.eucalyptus.com/docs/eucalyptus/4.0.1/index.html#shared/euca2ools_section.html) (euca2ools) as often used with the Amazon public cloud

All of these interfaces are available in lxplus (SLC6)  and lxplus7 (CC7). There are also versions available for standalone Mac, Windows and Linux machines.

The tools can be compared as follows:

In general, if you are looking for an environment which is like a physical box registered on the CERN network, the unified openstack CLI is probably the most appropriate tool. If you are looking for a single set of tools which could also run on EC2 clouds such as Amazon, euca2ools can be used (or one of the programming APIs listed in the advanced section).

**Note: ** Some EC2 functions in euca2ools do not work on OpenStack. The validated commands are listed [here](https://wiki.openstack.org/wiki/Nova/APIFeatureComparison#Amazon_EC2_API_Compatability).

| Feature | Unified CLI | Other OpenStack CLIs | Euca2ools |
| ------- | ----------- | -------------------- | --------- |
| Allows you to choose the hostname of your VM | Yes | Yes | No |
| Compatible with non-OpenStack clouds | No | No | Yes |
| Time to create a virtual machine | Default is 1-10 minutes waiting for DNS update. Options for faster creation are available in [creation time properties](http://clouddocs.web.cern.ch/clouddocs/using_openstack/properties.html).|As unified CLI|1 minute|
| DNS Entry | Yes | Yes | No - IP Address is allocated, DNS name cannot be changed |
| IPv6 option | Yes | Yes | No |
| Kerberos Server Tab (so you can use AFS or Windows services) | Yes | Yes | No |
| X.509 Host Certificate | Yes | Yes | No |
| Contextualisation using cloud-init or amiconfig | Yes | Yes | Yes |
| Outgoing connectivity (i.e. can connect to services on the Internet and LCG networks) | Yes | Yes | Yes |
| Incoming Services (i.e. can support users outside of CERN trying to access the services on the VM) | Not by default but can be requested through LanDB | As for unified CLI | Not by default and not recommended. |
| Supports X.509 or Kerberos client authentication | Yes | No | No |

The detailed functionality can be found in the [OpenStack documentation](https://wiki.openstack.org/wiki/OpenStackClient/Commands)

To install OpenStack tools on your own client machine, please follow Installing OpenStack tools page ([Linux](http://clouddocs.web.cern.ch/clouddocs/clients/linux_client_installation.html), [Mac](http://clouddocs.web.cern.ch/clouddocs/clients/mac_os_x.html) and [Windows](http://clouddocs.web.cern.ch/clouddocs/clients/windows.html)).
