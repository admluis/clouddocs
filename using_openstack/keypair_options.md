# Keypair options
Keypairs provide the method to authenticate when logging into a Linux VM. There are several options for how to create them.
## Using your lxplus SSH key
As described in [the tutorial](../tutorial/create_your_openstack_profile.md), you can use your lxplus SSH key as your keypair. This will allow you to ssh directly into the host using ssh *adminuser*@*vm* (where adminuser is the administrative user of the VM such as root and vm is the host).

## Using a new keypair
A new keypair can also be created for use, such as with a dedicated service account or daemon.

```
$ openstack keypair create robotkey > ~/.ssh/robotkey
$ chmod 600 ~/.ssh/robotkey
```

The private key is saved to ~/.ssh/robotkey and can then be used for VM creation (using the ```--key-pair``` argument on the command line) and for ssh using 
```
$ ssh -i ~/.ssh/robotkey *adminuser*@*vm*
```




