# Backups and snapshots

There is no integrated automated backup as part of the cloud. For many configurations, data is stored on reliable data stores such as

  * [Database-on-Demand](http://information-technology.web.cern.ch/services/database-on-demand)
  * Oracle
  * [AFS](http://information-technology.web.cern.ch/services/afs-service)
  * DFS

These services will arrange backup and monitoring of the data stores with the virtual machines being stateless clients which can be re-created rapidly using a tool such as Puppet.

In some cases, there is a desire for backup and restore of data stored on a virtual machine. A number of approaches can be used to cover these needs.

Snapshotting also provides the possibility to clone a VM such as for testing or a production upgrade where you clone the current production to a new VM, upgrade and then adjust an Ip alias to point to the new VM.

## Locally installed backup client

The [IT Backup Service](http://cern.ch/backup) provides a facility to perform backups within the virtual machine by installing a client program and providing the list of directories to save.

In particular, if Puppet is used to configure the backup client, this can be automated as described in the [backup user guide](https://information-technology.web.cern.ch/book/cern-backup-and-restore-service-user-guide/getting-started/client-installation/configure-tsm).


## Snapshots for rollback & rebuild

Typical use cases for a rollback and rebuild is a test environment with a reference configuration for the start of the tests. The test can be executed, checked and then the original state recovered.

   * Create a snapshot file of the virtual machine into glance for the reference configuration at the start of the test. This snapshot can then be used later for the rebuild reinstallation.
   * Use ```openstack server rebuild``` to roll back to the same state with same IP and hostname. This is much faster than deleting the old server and creating a new one with the same name (of the order of 2-3 minutes)

**Note:** During the time it takes to do the snapshot, the machine can become unresponsive. It is recommended to resync the VM clocks using the NTP daemon or running ntpdate after the snapshot has completed.

```
$ openstack server image create --name snap20150601 --wait tim-centos7-143
Finished
$ openstack image show snap20150601
```

Using this snapshot, the VM can be rolled back to the previous state with a server rebuild.

```
$ openstack server rebuild --image snap20150601 tim-centos7-143
```


## Snapshots for testing and cloning

In this use case, a VM is being used for a production service and you need to test a new software version. If your VM is under control of a configuration management system such as Puppet, it is recommended to create a new instance from the configuration templates.

The procedures below can be executed on an aiadm or lxplus class machine in order to use the sysprep tools.

Cloud procedures can be used to

   * Snapshot a virtual machine
   * Download the image file
   * Clean the image of MAC addresses etc.
   * Upload the cleaned image
   * Create a new VM from the cleaned image

```
$ openstack server image create --name snap20150601 --wait tim-centos7-143
$ openstack image save --file /tmp/snap20150601.qcow2 snap20150601
```

Running the sysprep tools on the snapshot removes the custom host information such as the hardware addresses and unique identifiers. This is necessary to avoid conflicts with the currently running VM when a new clone of the VM is created.

```
$ virt-sysprep -a /tmp/snap20150601.qcow2
Examining the guest ...
Performing "yum-uuid" ...
Performing "utmp" ...
Performing "udev-persistent-net" ...
Performing "sssd-db-log" ...
Performing "ssh-userdir" ...
Performing "ssh-hostkeys" ...
Performing "smolt-uuid" ...
Performing "script" ...
Performing "samba-db-log" ...
Performing "rpm-db" ...
Performing "rhn-systemid" ...
Performing "random-seed" ...
Performing "puppet-data-log" ...
Performing "pam-data" ...
Performing "package-manager-cache" ...
Performing "pacct-log" ...
Performing "net-hwaddr" ...
Performing "net-hostname" ...
Performing "mail-spool" ...
Performing "machine-id" ...
Performing "logfiles" ...
Performing "hostname" ...
Performing "firstboot" ...
Performing "dovecot-data" ...
Performing "dhcp-server-state" ...
Performing "dhcp-client-state" ...
Performing "cron-spool" ...
Performing "crash-data" ...
Performing "blkid-tab" ...
Performing "bash-history" ...
Performing "abrt-data" ...
Performing "lvm-uuids" ...
```

Now upload the cleaned image to Glance so a new VM can be created from it.

```
$ openstack image create --file /tmp/snap20150601.qcow2 --disk-format=qcow2 --container-format=bare clean20150601
```

And then a new server can be created from the clean image.

```
$ openstack server create --key-name lxplus --flavor m1.small --image clean20150601 tim-centos7-clone-143
```

## Snapshots to move between projects or clouds

In some scenarios, you need to move a virtual machine between projects. Typical example would be if you have accidentally created a VM in your Personal project and actually need it for production usa in a shared project.

Other scenarios would be if you have a working virtual machine in one OpenStack cloud and you needed to move it to another.

The following procedure is available for Linux virtual machines, for the Windows VMs please open a request ticket via SNOW.

The steps are
   * Log into the source VM as root and run the following to remove information about the host machine. These files may not exist but they need to be removed if they do. Alternatively, you can use the sysprep tool on the image.

```
$ /bin/rm /etc/udev/rules.d/70-persistent-net.rules
$ /bin/rm /lib/udev/write_net_rules
```

   * Authenticate to the source project
   * Shutdown the source virtual machine
   * Make a snapshot of the source virtual machine
   * Download the snapshot to a file
   * If you want to keep the same hostname, check the snapshot is correct (i.e. it seems a reasonable size) and then delete the source VM. You will need to wait for 15 minutes or so until the DNS entry has expired. Check this with the 'host' command
   * Authenticate to the new project you want to create the destination VM in
   * Upload the snapshot file to Glance in the new project
   * Create a new server from the snapshot

## Rename a virtual machine

As above, the steps are similar

   * Authenticate to the source project
   * Shutdown the source virtual machine
   * Make a snapshot of the source virtual machine
   * Download the snapshot to a file
   * Sysprep the snapshot to clean the hostname information
   * Upload the new image to Glance
   * Create a new server from the snapshot file with the new hostname
   * (If all is working OK), delete the old VM

## Snapshots for archive