# Availability Zones

An availability zone is a way in which the user can specify a particular “location” in which a host should boot. The most common usage for availability zones is to group together servers in terms of availability but other features could be external network connectivity or redundant power. When a nova boot command is issued, an --availability-zone can be provided to specify which zone to start the VM in.
The current zones available can be found running nova availability-zone-list. The results show the name of the availability zones currently available such as 

```
nova availability-zone-list
```

This will list a set of zones of the format below

  * cern-geneva-*letter* are zones in the CERN Meyrin data centre. Virtual machines allocated to hypervisors on the *cern-geneva-a* zone will not share the same hypervisors as those in the *cern-geneva-b* zone.

  * cern-wigner-*letter* are zones in the CERN Budapest data centre.

  * nova are zones which are placed in the default zone.

The availability zone to use can be specified by ```--availability-zone <zone-name>``` on the command line of the openstack CLI.

The default availability zone is one of the cern-geneva-a, -b or -c zones.

