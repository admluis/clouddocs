# VM deletion

VMs can be deleted using either the OpenStack dashboard or via the ```openstack server delete``` command.

**Note:** This will immediately terminate the instance, delete all content of the virtual machine and erase the disk, This operation is not recoverable.

**Note:** If the VM is puppet managed, please use the appropriate ai-kill-vm command from the configuration management tools.

There are other options available if you wish to keep the virtual machine for future usage. These do, however, continue to use quota for the project even though the VM is not running.

  * Suspend the VM which saves the state of the VM to disk so it could be restarted later
  * Snapshot the VM to keep an offline copy of the virtual machine

If, however, the virtual machine is no longer required and no data on the associated system or ephemeral disk needs to be preserved, the following command can be run:

```
$ openstack server delete timtest23
```

The quota associated with this virtual machine will be returned to the project within an hour. If the quota does not return, please raise a ticket.
 