# Properties

## <a id=novaproperties>Creation time properties</a>

The following properties can only be set during the `openstack server create` process. The term metadata is also used to describe these variables and values.

```
openstack server create --property key=value ...
```

| Property | Description |
| -------- | ----------- |
|cern-services|Set to true (default) or false to determine whether the additional integration into the CERN services is required. These services include a DNS entry using the server name specified so the name matches the IP address in LANDB, Active directory so that kerberos identity is established before booting and an X.509 certificate for the host is available. However, this also makes the boot time significantly longer.<p>**Note:** This should not be used if an AFS client or other Kerberos/Grid tools are needed. Standard images such as the "CC7 Extra" images should not be used with this option. |
|cern-update-hostname|If cern-services is set to false, the default behaviour is to asychronously update LanDB to set the hostname to the one in the OpenStack CLI. cern-update-hostname can be set to false so that no LanDB hostname update will be made and the original hostname as defined in LanDB will be used. The latter option speeds up booting.<p>**Note:** If this is set to true, the hostname may be updated only after ```cloud-init``` has run which will need to be handled by the system configuration.|
|cern-datacentre|Can be set to either *meyrin* or *wigner* in order to specify where the virtual machine should be created. Currently, this defaults to meyrin (but this may be reviewed in future). |

## Other properties
A virtual machine's configuration can be adjusted during its lifetime. The following properties can be changed in real time.

Metadata options can be passed to the image using the ```--property``` option such as

```
$ openstack server set --property landb-description="document server" timdoc143
```


| Property | Description |
| -------- | ----------- |
|description|A text string describing the VM. This can be used to provide more details on the purpose of the VM. The default is no description.|
|landb-description|Sets the LanDB description field to the value provided. By default, this string is empty.|
|landb-mainuser|A User name or e-group name. The primary user of the virtual machine. The syntax for the field is the CERN user name (such as timbell) and not the e-mail address. e-groups are specified by giving the name of the group (i.e. the mailing list address without @cern.ch).|
|landb-responsible|A User name or e-group name. The person responsible for the virtual machine. The syntax is as for mainuser.|
|landb-os|The operating system to be stored in LanDB. This is a free format field.|
|landb-osversion|The operating system version to be stored in LanDB. This is a free format field.|
|landb-alias|Comma separated list of alias names. The list of DNS aliases that are to be added to the host.<p>**Note:** The previous alias list is overwritten by this command. Thus, if you wish to append a new alias, it is necessary to get the value using nova show and then add the new alias.|
|landb-ipv6ready|A Boolean (true false). Set to true if the applications running on the VM can support IPv6. This is false by default while testing of applications continues. For details of how to configure your VM with IPv6, see [Enabling IPv6 ](http://clouddocs.web.cern.ch/clouddocs/networking/ipv6.html).|


## Clearing properties
If a value needs to be cleared, this can be done using the ````openstack server unset```` command. For example, the following removes the LanDB description value. This setting is especially useful where LanDB values need resetting to their defaults.

```
$ openstack server unset --property landb-description timdoc144
```