# VM Creation
Creating virtual machines needs a combination of

* An image from which the VM should be created
* A flavor defining the size of the virtual machine
* A keypair to define the authentication for logging in as administrator

Optionally, you can also supply

* Contextualisation to customise the virtual machine contents after creation
* Parameters to configure the virtual machine context

## Simple virtual machine creation
Creating a virtual machine can be done using the CLI as described in the [tutorial](http://clouddocs.web.cern.ch/clouddocs/tutorial/openstack_command_line.html).

The available images are described in [Using images](http://cern.ch/clouddocs/details/images.html) chapter.

## Options

For details on options, see [Properties](http://clouddocs.web.cern.ch/clouddocs/using_openstack/properties.html).

## Fast VM creation

The default VM creation allows a hostname to be chosen and fully integrates this server into CERN's infrastructure (such as domain name service and Kerberos). If these steps are not required, virtual machines can be created more quickly (5 minutes rather than 15) as follows using the [properties](http://cern.ch/clouddocs/using_openstack/properties.html) below.

**Note:** The name of the host on the command line will not be used for the final virtual machine

```
openstack server create --property cern-services=false --property cern-update-hostname=false --flavor m1.small --image 1c5607b5-d373-4696-ad37-e2dc764f99be --key-name lxplus timquick143
```

In this case, the name for the server does not taken from the command. The exact hostname can be retrieved from ```openstack server show``` on the instance ID from the create command. The example below has the hostname chosen as ZLMXIA33RDD.

```
$ openstack server show aff506fd-58cf-4770-8202-4382f4beaefd
+--------------------------------------+--------------------------------------------------------------------------------+
| Field                                | Value                                                                          |
+--------------------------------------+--------------------------------------------------------------------------------+
| OS-DCF:diskConfig                    | MANUAL                                                                         |
| OS-EXT-AZ:availability_zone          | cern-geneva-c                                                                  |
| OS-EXT-STS:power_state               | 1                                                                              |
| OS-EXT-STS:task_state                | None                                                                           |
| OS-EXT-STS:vm_state                  | active                                                                         |
| OS-SRV-USG:launched_at               | 2015-01-12T08:38:57.000000                                                     |
| OS-SRV-USG:terminated_at             | None                                                                           |
| accessIPv4                           |                                                                                |
| accessIPv6                           |                                                                                |
| addresses                            | CERN_NETWORK=188.184.185.200, 2001:1458:301:10::100:c2                         |
| config_drive                         |                                                                                |
| created                              | 2015-01-12T08:37:11Z                                                           |
| flavor                               | m1.small (2)                                                                   |
| hostId                               | 4a00c143c0df137dde20d53a95fe9399acb51b5f8ce56d862621cebe                       |
| id                                   | aff506fd-58cf-4770-8202-4382f4beaefd                                           |
| image                                | CC7 Server - x86_64 [2014-10-31] [TEST] (1c5607b5-d373-4696-ad37-e2dc764f99be) |
| key_name                             | lxplus                                                                         |
| name                                 | ZLMXIA33RDD                                                                    |
| os-extended-volumes:volumes_attached | []                                                                             |
| progress                             | 0                                                                              |
| properties                           | cern-services='false', cern-update-hostname='false'                            |
| security_groups                      | [{u'name': u'default'}]                                                        |
| status                               | ACTIVE                                                                         |
| tenant_id                            | 841615a3-ece9-4622-9fa0-fdc178ed34f8                                           |
| updated                              | 2015-01-12T08:38:57Z                                                           |
| user_id                              | timbell                                                                        |
+--------------------------------------+--------------------------------------------------------------------------------+
```

## Errors

The following error messages can be produced by the commands for VM creation.

| Error | Description |
| ----- | ----------- |
| ERROR: You must provide a username via either --os_username or env[OS_USERNAME] | The openrc file was not sourced correctly. Follow the procedures defined [environment options](http://clouddocs.web.cern.ch/clouddocs/using_openstack/environment_options.html)  |
|ERROR: Invalid OpenStack Nova credentials.|Check the password entered when the openrc file is sourced. The CERN password should be used for the account you are logging in as.|
|BadRequest: n/a (HTTP 400)<p>ERROR: n/a (HTTP 400)|If the clients are running on your own machine, make sure that you are using a version of OpenStack tools from the ai6 CERN repository. Installing the client tools is documented in the [clients](http://clouddocs.web.cern.ch/clouddocs/clients/linux_client_installation.html). If the problem continues with the correct clients, please raise a support request.|
|ERROR: Quota exceeded for cores:|The project you are running in has now reached its maximum number of compute cores. Please review the usage of the project and if required, raise a support request for further resources. Personal project quotas are fixed.|
|ERROR: Invalid key_name provided.|The argument to the key-name parameter was incorrect. Do not add the .pem extension for the file name.|
|Error: Instance type's disk is too small for requested image. (HTTP 400)|The flavor chosen has a disk which is not big enough for the image requested. Select a larger flavor and try again.|
|ERROR: [Errno 1] _ssl.c:492: error:14090086:SSL routines:SSL3_GET_SERVER_CERTIFICATE:certificate verify failed|The certificate settings were incorrect. The variable OS_CACERT should not be set in the environment (unset OS_CACERT; export OS_CACERT to clear it).If the problem persists, please raise a service desk ticket.|
|ERROR: [Errno 8] _ssl.c:492: EOF occurred in violation of protocol|As above|
|ERROR: Instance name is not a valid hostname|The server name will be used as the DNS entry for the VM. The server name provided is not a valid name for a TCP/IP hostname. Underscores and other special characters are not permitted in hostnames.|
|Hostname already in DNS (HTTP 400)|The name of the server being requested is registered in the DNS. If you have recently deleted a server of the same name, it is necessary to wait for 15 minutes for the DNS to be refreshed. Otherwise, the current owner of the hostname can be found in the [network database](http://cern.ch/network).|
|ERROR: cliff.app The resource could not be found. (HTTP 404)|Check the environment variables set in your profile. This error message can appear if you do not have the OS_IDENTITY_API_VERSION variable set correctly.|
|ERROR: 'NoneType' object has no attribute 'rstrip'|The Kerberos and X.509 endpoints should only be used with the unified command line tool *openstack* rather than the nova, glance, cinder, etc commands. If you wish to use the other commands, you need to source the profile which sets OS_PASSWORD as described in [Environment Options](http://clouddocs.web.cern.ch/clouddocs/using_openstack/environment_options.html).|
| The requested availability zone is not available|The --availability-zone parameter is not set correctly. Check the [list of available zones](http://clouddocs.web.cern.ch/clouddocs/using_openstack/availability_zones.html).|


## VM Error Status

There are scenarios where the ```openstack server create``` command is accepted but there is an error in the actual creation of the VM. These errors are given in the output of the ```openstack server show``` command.

```
| fault                  | {u'message': u'NoValidHost', u'code': 500, u'created': u'2013-05-27T11:31:39Z'} |
```


| Fault Message| Description |
| ------------ | ----------- |
|NoValidHost|A hypervisor could not be found where the request could be placed. There may be different reasons for this, please raise a ticket if needed.|
|CernException|An error ocurred during the CERN specific part of allocating a virtual machine. Please raise a ticket for assistance.|
|NoMoreFixedIps_Remote|An error ocurred during the CERN specific part of allocating a virtual machine. Please raise a ticket for assistance.|
|Invalid hostname.|The hostname used is not a valid DNS name. An alternative server name should be chosen.|

