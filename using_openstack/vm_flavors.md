# VM flavors
Virtual machine flavors allow you to choose the size of your virtual machine.
The selection available can be shown using

```
$ openstack flavor list
+----+------------+------+------+-----------+------+-------+-------------+-----------+-------------+
| ID | Name       |  RAM | Disk | Ephemeral | Swap | VCPUs | RXTX Factor | Is Public | Extra Specs |
+----+------------+------+------+-----------+------+-------+-------------+-----------+-------------+
| 1  | m1.tiny    |  512 |    0 |         0 |      |     1 |         1.0 | True      |             |
| 2  | m1.small   | 2048 |   20 |         0 |      |     1 |         1.0 | True      |             |
| 3  | m1.medium  | 4096 |   40 |         0 |      |     2 |         1.0 | True      |             |
| 4  | m1.large   | 8192 |   80 |         0 |      |     4 |         1.0 | True      |             |
| 50 | win.small  | 2048 |   60 |         0 |      |     1 |         1.0 | True      |             |
| 51 | win.medium | 4096 |   80 |         0 |      |     2 |         1.0 | True      |             |
| 52 | win.large  | 8192 |  120 |         0 |      |     4 |         1.0 | True      |             |
+----+------------+------+------+-----------+------+-------+-------------+-----------+-------------+
```

The important fields are

| Field | Description |
| ----- | ----------- |
| RAM | Memory size in MB |
| Disk | Size of disk in GB. 0 means that the disk is the same size as the image |
| Ephemeral | Size of a second disk. See the [ephemeral disks section](http://clouddocs.web.cern.ch/clouddocs/details/ephemeral.html) for more details. |
| Swap | Size of swap space. No value means no swap volume is defined |
| VCPUs | Number of virtual cores |


For more details on flavors, see the [detailed chapter](/clouddocs/details/flavors.html).

## Resizing VMs to a new flavor

**Note:** Resizing a VM will make it unavailable for around 15 minutes as the VM resized.

Before resizing,

   * Check the current size of the VM using ```openstack server show```. This is shown on the line marked 'flavor'
   * Select a flavor for the new size using ```openstack flavor list```.

Run

```
$ openstack server resize --flavor m1.large --wait tim-centos7-143
```

Check the state of the virtual machine using ```openstack server show``` until it is in the status VERIFY_RESIZE. During the operation, the status line will display RESIZE. This can take around 15 minutes.

Then log in to the VM and confirm that the resize operations have worked. It may take a minute before ssh can connect to the VM. The [console commands](http://clouddocs.web.cern.ch/clouddocs/using_openstack/console_access.html) can be used to see when sshd has started again.

Then run the following to accept the change and obtain the status ACTIVE.

```
$ openstack server resize --verify tim-centos7-143
```


To roll back, use

```
$ openstack server resize --revert tim-centos7-143
```


If the disk is a larger size in the new flavor, you will need to resize the disk using 
one of the [guest specific procedures](../guest_specific_procedures/README.md)