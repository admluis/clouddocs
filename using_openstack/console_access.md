# Console access

Virtual machine consoles can be accessed through both the command line tools and the dashboard.

By default, the administrators of a project can access the console of a virtual machine. If there is need to expand these
rights further, the permissions can be adjusted for shared projects using the account portal as described in [Project Roles](http://clouddocs.web.cern.ch/clouddocs/projects/project_roles.html).

## Using Horizon with a browser

Access to the graphical console can be made via Horizon. Select the node on the initial page you wish to connect to and then the console tab. A screen as follows should appear

![Console](http://cern.ch/clouddocs/images/webconsole.png)

The text console can be accessed using the Log tab. This shows the text output including history.

## Using VNC or RDP via a URL

The URL for the console can be retrieved via the unified CLI.

```
$ openstack console url show timpupc7
+-------+-----------------------------------------------------------------------------------------+
| Field | Value                                                                                   |
+-------+-----------------------------------------------------------------------------------------+
| type  | novnc                                                                                   |
| url   | https://openstack.cern.ch:6080/vnc_auto.html?token=ea8f9a2c-8396-442b-86f7-675e2cab026b |
+-------+-----------------------------------------------------------------------------------------+```

A browser can then be pointed directly at the URL to retrieve the console. This can be used if you need to embed the console access into a web interface.

## Console log Using command line tools

Unlike the dashboard access, the command line tools can access the console history in text. This is particularly useful for Linux virtual machines where crash dump output can be checked.

```
$ openstack console log show timdoc143
```



**Note: ** The console log is reset when a VM is hard rebooted.

