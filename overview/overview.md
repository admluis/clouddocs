# Overview of the CERN cloud

## What is cloud computing ?

Cloud computing provides an efficient pooling of on-demand, self-managed virtual infrastructure, consumed as a service.

There are 3 layers according to the standard definitions

![](http://cern.ch/clouddocs/images/Cloud_computing_layers.png)

CERN's cloud is an Infrastructure-as-a-Service cloud which provides

   * Self Service - use a [web portal](http://cern.ch/openstack) to create, manage and delete virtual machines.
   * Programmable - operations can be performed through command line tools or programming language libraries.
   * Elasticity - you can create virtual machines up to your quota and delete them when no longer required.
   * Efficient - users can have small virtual machines which are packed onto the physical hardware to provide better resource utilisation.

One key aspect is that production applications should be structured to tolerate from single points of failure. From power failures, network to hardware issues, software redundancy using load balancers and reliable data stores has been shown to produce more stable applications.

## What is OpenStack ?

[OpenStack](http://openstack.org) is an open source cloud computing software

The CERN private cloud is based on OpenStack and configured to be integrated with the CERN network and authentication services.

![OpenStack](http://cern.ch/clouddocs/images/openstack-software-diagram.png)

For more information, see [Additional Information](/clouddocs/additional/README.html)





