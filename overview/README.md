# CERN cloud concepts

This chapter describes the key concepts behind the CERN cloud.

It describes

   * A quick overview of the system
   * A summary of the basic concepts
   * Advice on how to use the cloud


