# Getting help

## Service Status

Incidents and upcoming changes with the cloud service are reported using the IT tools.

Availability of the cloud service can be found under Server Provisioning on the [IT Service Display](https://cern.service-now.com/service-portal/sls.do?banner=false).

Upcoming changes and incidents are reported to the [IT Status Board](http://cern.ch/itssb).

## Reporting problems

For reporting problems or asking for assistance, please contact the service desk at https://cern.service-now.com/service-portal/. Checking the [IT SSB](http://cern.ch/itssb) and the [IT Service Display](https://cern.service-now.com/service-portal/sls.do?banner=false) will help to see if the problem is known.

For raising a ticket specifically for the OpenStack cloud, you can use the [direct link](https://cern.service-now.com/service-portal/function.do?name=cloud-infrastructure&s=cloud).

## General OpenStack usage questions

The OpenStack community maintains a methods for asking general OpenStack usage questions. These can often be useful for searching if end user problems have been seen before by others.

| Information | Description |
| -- | -- |
| [OpenStack documentation](http://docs.openstack.org) | OpenStack maintains a set of manuals for generic OpenStack usage. The [end user manuals](http://docs.openstack.org/user-guide/content/) cover the functionality in some depth although not all features may be implemented at CERN |
| [ask.openstack.org](http://ask.openstack.org) | General end user how to questions and answers |
| [OpenStack mailing list](https://wiki.openstack.org/wiki/Mailing_Lists) | A number of lists from general questions, how to operate OpenStack and the developer mailing lists for those contributing to the code |

