# Concepts

## OpenStack Components

OpenStack consists of a number of components. The following ones are installed at CERN. Each component has a general description and also the name used in development (for example, the OpenStack compute service is code named Nova). These terms are used interchangeably.

| Component | Description |
| -- | -- |
| OpenStack Compute (Nova)|OpenStack compute is the component which allows the user to create and manage virtual servers using the machine images. It is the brain of the Cloud. OpenStack compute provisions and manages large networks of virtual machines.|
|Block Storage (Cinder)|This component provides persistent block storage to running instances. The flexible architecture makes creating and managing block storage devices very easy.|
|Identity Service (Keystone)|This provides a central directory of users mapped to the OpenStack services. It is used to provide an authentication and authorization service for other OpenStack services.|
|OpenStack Image Service (Glance)|This provides the discovery, registration and delivery services for the disk and server images. It stores and retrieves the virtual machine disk image.|
|OpenStack Telemetry Service (Ceilometer)|It monitors the usage of the Cloud services and decides the billing accordingly. This component is also used to decide the scalability and obtain the statistics regarding the usage.|
|Dashboard (Horizon)|This component provides a web-based portal to interact with all the underlying OpenStack services, such as NOVA, Neutron, etc.|
|Orchestration (Heat)|This component manages multiple Cloud applications through an OpenStack-native REST API and a CloudFormation-compatible Query API. It will become available to CERN cloud users in the first half on 2015.|

## Project

A container used to group a set of resources such as virtual machines, volumes and images with the same access rights and quota.

## Quota

A per-project limit such as the total number of cores or RAM permitted for a set of virtual machines.

## Flavor

A Flavor is the definition of the size of a virtual machine and its characteristics (such as 2 core virtual machine with 8 GB of RAM).

## Image

A virtual machine image is a single file that contains a virtual disk that has a bootable operating system installed on it. Images are used to create virtual machine instances within the cloud.

## Volume

Volumes are block storage devices that you attach to instances to enable persistent storage. You can attach a volume to a running instance or detach a volume and attach it to another instance at any time. You can also create a snapshot from or delete a volume.

## Snapshot

A snapshot provides a copy of a currently running VM or volume which can be stored into an external service such as Glance.


