# Advanced Concepts

## Resource Group

Way to automate the creation of resources.
Create a server (server.yaml):

```
  server:
    type: OS::Nova::Server
    properties:
      image: cc7
      flavor: m1.medium
```

Create three servers at once (stack.yaml):

```
resources:
  rg:
    type: OS::Heat::ResourceGroup
    properties:
      count: 3
      resource_def: {type: server.yaml}

```

## Configure a server

Heat templates allows to insert user data via cloud-init:

```
  server01:
    type: OS::Nova::Server
    properties:
      image: cc7
      flavor: m1.medium
      user_data_format: RAW
      user_data:
        str_replace:
          template: |
            #!/bin/sh
            yum install -y httpd
            service httpd start
            iptables -I INPUT 4 -m state --state NEW -p tcp --dport 80 -j ACCEPT
            service iptables save
            service iptables restart

```

## Startup Order

* Heat mechanism to notify when a resource has finished all its operations
* Uses a web hook (via curl) to notify heat of an event
* The notification mechanism coupled together with a dependency on the resource allows to make a startup order

Create a database server:

```
  database:
    type: OS::Nova::Server
    properties:
      key_name: my_key
      image: "CC7 Extra - x86_64 [2015-02-10]"
      flavor: m1.small
      metadata: {"cern-services": "false"}
```

Create web hook to notify heat of an event:

```
  database_wait:
    type: OS::Heat::WaitCondition
    depends_on: database
    properties:
      handle: {get_resource: database_handle}
      timeout: 1000

  database_handle:
    type: OS::Heat::WaitConditionHandle
```

Notify Heat that database server finished:

```
      user_data_format: RAW
      user_data:
        str_replace:
          template: |
            #!/bin/sh
            wc_notify --data-binary '{"status": "SUCCESS"}'
          params:
            wc_notify: { get_attr: ['database_handle', 'curl_cli'] }
```

Web Server depends on a Database Server:

```
  webserver:
    type: OS::Nova::Server
    depends_on: database_wait
    properties:
      key_name: my_key
      image: "CC7 Extra - x86_64 [2015-02-10]"
      flavor: m1.small
      metadata: {"cern-services": "false"}

```

More Information: [Manage instance startup order in OpenStack Heat Templates](http://blog.zhaw.ch/icclab/manage-instance-startup-order-in-openstack-heat-templates/)


## AutoScalling

* Heat allows to build a stack that automatically reacts to events through time
* Uses ceilometer alarms to monitor resources

Create ceilometer alarm for CPU utilization:

```
 cpu_alarm_high:
    type: OS::Ceilometer::Alarm
    properties:
      description: Scale-up if the average CPU > 50% for 1 minute
      meter_name: cpu_util
      statistic: avg
      period: 60
      evaluation_periods: 1
      threshold: 50
      alarm_actions:
        - {get_attr: [web_server_scaleup_policy, alarm_url]}
      matching_metadata: {'metadata.user_metadata.stack': {get_param: "OS::stack_id"}}
      comparison_operator: gt
```

Create a heat scale up policy:

```
 web_server_scaleup_policy:
    type: OS::Heat::ScalingPolicy
    properties:
      adjustment_type: change_in_capacity
      auto_scaling_group_id: {get_resource: web_server_group}
      cooldown: 60
      scaling_adjustment: 1

```

## Healing

* Heat allows to define a mechanism for healing resources
* Alarms trigger creation and deletion of faulty resource:
 * Ceilometer alarm
 * Use a simple web hook via curl

Create the healing mechanism:

```
 serverRestarter:
    type: OS::Heat::HARestarter
    properties:
      InstanceId: {Ref: server}
```

Create the ceilometer alarm that triggers the healing mechanism:

```
    type: OS::Ceilometer::Alarm
    properties:
      description: Scale-up if the average CPU > 50% for 1 minute
      meter_name: cpu_util
      statistic: avg
      period: 60
      evaluation_periods: 1
      threshold: 50
      alarm_actions:
        - {get_attr: [serverRestarter, AlarmUrl]}
      matching_metadata: {'metadata.user_metadata.stack': {get_param: "OS::stack_id"}}
      comparison_operator: gt
```