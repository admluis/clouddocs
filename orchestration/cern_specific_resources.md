# CERN-Specific Resources

Set of Heat resources that provide better integration with CERN infrastruture.

## Web SSO

Heat resource to register a web site with SSO.

```
parameters:
  instance_name:
    type: string
    description: VM Name
    constraints:
    - allowed_pattern: "[a-zA-Z0-9-]+"

resources:
  sso:
    type: CERN::Web::SSO
    properties:
      host: { get_param: instance_name }
      auth: ["HEP Trusted","CERN Registered"] # optional
      
  server:
    type: OS::Nova::Server
    depends_on: sso
    properties:
      name: { get_param: instance_name }

```

## Puppet

Pre-requisites:

* For adding your new puppet VM you need to allow heat to create new VMs on foreman. For that just add the user "heatfm" to the egroup managing your hostgroup.

Resource needed to create a nova server with puppet configuration.

```
resources:
  puppet:
    type: CERN::CM::Puppet
    properties:
      fqdn: {get_param: instance_name}
      hostgroup: "test_hg"
      environment: "env_hg" # optional, if not set goes to "production" environment

  server:
    type: OS::Nova::Server
    depends_on: puppet
    properties:
      name: { get_param: instance_name }
      key_name: my_key
      image: cc7-image
      flavor: m1.small
      user_data_format: RAW
      user_data: { get_attr: [puppet, user_data] } # Use userdata created by puppet resource
```
