# Create a Heat template

## Overview

Heat consumes templates written in "Heat Orchestration Template" (HOT) format.

* Written with YAML syntax
* Fully documented in the [HOT specification](http://docs.openstack.org/developer/heat/template_guide/hot_spec.html)

Heat can also use templates in AWS CloudFormation template format.

Reference: [Heat Templates Repository](https://github.com/openstack/heat-templates)


## Complete Example

tl;dr: Here you can find a complete working template for CERN Cloud infrastructure.

```
heat_template_version: 2013-05-23

description: Simple template to deploy a single compute instance

parameters:
  instance_name:
    type: string
    description: VM Name
    constraints:
    - allowed_pattern: "[a-zA-Z0-9-]+"
resources:
  heatinstance:
    type: OS::Nova::Server
    properties:
      name: { get_param: instance_name }
      key_name: my_key
      image: "CC7 Extra - x86_64 [2015-02-10]"
      flavor: m1.medium
      metadata: {"cern-services": "false"}
```

## Structure

Templates have three sections:

```
# This is required.
heat_template_version: 2013-05-23

parameters:
  # parameters go here

resources:
  # resources go here (this section is required)

outputs:
  # outputs go here

```

## Resources

The resources section specifies what resources Heat should create:

```
resources:
  my_resource_id:
    type: a_resource_type
    properties:
      property1: ...
      property2: ...
```

Resources can depend on other resources. Explicitly:

```
my_server:
  type: "OS::Nova::Server"
  depends_on: my_network
```

## Resource Documentation

Online documentation includes list of [available resource types](http://docs.openstack.org/developer/heat/template_guide/openstack.html). You should refer to that document whenever you need a new resource.

You can also query Heat for information about available resource types:

```
$ heat resource-type-list
+------------------------------------------+
| resource_type                            |
+------------------------------------------+
[...]
| OS::Nova::FloatingIP                     |
| OS::Nova::FloatingIPAssociation          |
| OS::Nova::KeyPair                        |
| OS::Nova::Server                         |
[...]
```

And for information about a specific type:

```
$ heat resource-type-template OS::Nova::Server
HeatTemplateFormatVersion: '2012-12-12'
Outputs:
  accessIPv4: {Description: The manually assigned alternative public IPv4 address
      of the server., Value: '{"Fn::GetAtt": ["Server", "accessIPv4"]}'}
Parameters:
  admin_pass: {Description: The administrator password for the server., Type: String}
Resources:
  Server:
    Properties:
      admin_pass: {Ref: admin_pass}
  Type: OS::Nova::Server
```

## Parameters

We can replace hardcoded values with parameters:

```
parameters:
  image:
    type: string
    default: "CC7 Extra - x86_64 [2015-02-10]"
    description: >
      image name or id used to boot our nova servers
```

Parameters can have:

* Types (string, number, comma_delimited_list, json, or boolean)
* Descriptions
* Constraints ("must be less than", "must be one of the following values", etc)
* Hints and help for UI display

Details in the [hot specification](http://docs.openstack.org/developer/heat/template_guide/hot_spec.html#parameters-section).

When creating a stack, parameters can be specified:

On the command line with -P:

```
heat stack-create -f my_stack.yaml  \
    -P "image=CC7 Extra - x86_64 [2015-02-10]" my_stack
```

On the command line using an environment file:

```
heat stack-create -f my_stack.yaml \
    -e my_env.yaml my_stack
```

An environment file is a YAML file with a parameters section containing values for parameters declared in your template:

```
parameters:
  image: "CC7 Extra - x86_64 [2015-02-10]"
```

## Outputs

Sometimes we want to extract information about a stack:

```
outputs:
  server_ip:
    description: fixed ip assigned to the server
    value: { get_attr: [my_server, first_address] }
```

These can be retrieved via *heat output-list* and *heat output-show* commands (and via API).