# Create a Heat stack

## Creation

Start a stack:

```
$ heat stack-create hot_simple -f prod_hot_simple.yaml -P "instance_name=novajuno01"
+--------------------------------------+------------+--------------------+----------------------+
| id                                   | stack_name | stack_status       | creation_time        |
+--------------------------------------+------------+--------------------+----------------------+
| 24fb1d3b-003c-4434-bdec-fe529aa4e709 | hot_simple | CREATE_IN_PROGRESS | 2015-04-28T14:39:15Z |
+--------------------------------------+------------+--------------------+----------------------+
```

Verify the stack has been created successfully:

```
$ heat stack-list
+--------------------------------------+------------+-----------------+----------------------+
| id                                   | stack_name | stack_status    | creation_time        |
+--------------------------------------+------------+-----------------+----------------------+
| 24fb1d3b-003c-4434-bdec-fe529aa4e709 | hot_simple | CREATE_COMPLETE | 2015-04-28T14:39:15Z |
+--------------------------------------+------------+-----------------+----------------------+
```

We can see the resources associated with this stack:

```
$ heat resource-list hot_simple
+---------------+--------------------------------------+------------------+-----------------+----------------------+
| resource_name | physical_resource_id                 | resource_type    | resource_status | updated_time         |
+---------------+--------------------------------------+------------------+-----------------+----------------------+
| heatinstance  | 38ed00b7-d7df-460b-8d8c-b9c61b197373 | OS::Nova::Server | CREATE_COMPLETE | 2015-04-28T14:39:16Z |
+---------------+--------------------------------------+------------------+-----------------+----------------------+
```

We can verify that this did, indeed, create a server:

```
$ nova list
+--------------------------------------+------------+--------+------------+-------------+--------------------------------------------------------+
| ID                                   | Name       | Status | Task State | Power State | Networks                                               |
+--------------------------------------+------------+--------+------------+-------------+--------------------------------------------------------+
| 38ed00b7-d7df-460b-8d8c-b9c61b197373 | novajuno01 | ACTIVE | -          | Running     | CERN_NETWORK=128.142.136.248, 2001:1458:301:9e::100:ea |
+--------------------------------------+------------+--------+------------+-------------+--------------------------------------------------------+
```

## Debug Failed Stack

If you see:

```
$ heat stack-list
+--------------------------------------+------------+---------------+----------------------+
| id                                   | stack_name | stack_status  | creation_time        |
+--------------------------------------+------------+---------------+----------------------+
| d3d5f8a9-38e8-41cd-b22f-15c9b8fbdb16 | hot_simple | CREATE_FAILED | 2015-04-29T08:11:44Z |
+--------------------------------------+------------+---------------+----------------------+
```

You can query Heat for information about the problem:

```
$ heat stack-show hot_simple
| stack_status         | CREATE_FAILED                                             |
| stack_status_reason  | Resource CREATE failed: BadRequest: Instance name is      |
|                      | not a valid hostname (HTTP 400) (Request-ID: req-         |
|                      | 57c3ac77-9f8b-4d87-9587-a2279b111ad4)                     |                                                                                 
```
