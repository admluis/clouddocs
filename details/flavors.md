# Flavors

A flavor defines the virtual machine size such as Number of virtual CPU cores Amount of memory Disk space (system disk, ephemeral disk and swap)

A standard set of flavors allows predictable distribution of applications across multiple hypervisors. These are based on the sizes and names available in Amazon EC2. They can be listed using the ```
openstack flavor list```
 command. The m1 names match equivalent configurations in Amazon. The win flavors are for Windows desktop and servers which require a larger system disk.

A disk size of 0 means that the size of the disk is the same as that in the image. For other cases, it may be necessary to resize the partitions.

| Name | Memory (MB) | Disk (GB) | Ephemeral (GB) | VCPUs (cores) |
| -- | -- | -- | -- | -- | -- |
|m1.small|2048|20|0|1|
|m1.medium|4096|40|0|2|
|m1.large|8192|80|0|4|
|win.small|2048|60|0|1|
|win.medium|4096|80|0|2|
|win.large|8192|120|0|4|

If you find that the specific configuration you require is not available in a shared project, please raise a [ticket](http://clouddocs.web.cern.ch/clouddocs/overview/getting_help.html) to the cloud infrastructure support team. Non-default flavors will not be added to Personal projects.

Common additional flavors are listed below and can be added to projects on request. The HEP flavors follow the 1 core, 2GB ratios and the cvm flavors correspond to the [CERNVM](http://cernvm.cern.ch) configurations with a small system disk and a larger ephemeral disk for the CVMFS cache.

| Name | Memory (MB) | Disk (GB) | Ephemeral (GB) | VCPUs (cores) |
| -- | -- | -- | -- | -- | -- |
|hep2.1|2048|70|20|1|
|hep2.2|4096|70|40|2|
|hep2.4|8192|70|80|4|
|cvm.medium|4096|1|90|2|
|cvm.large|8192|1|130|4|
|cvm.xlarge|16384|1|220|8|
