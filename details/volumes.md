# Storage and Volumes

There are several ways of handling disk storage in the CERN private cloud.

* System disks are the first disk in the flavor and are generally used to store 
  the operating system created from an image when the virtual machine is booted. 
  Note: on SLC6 Virtual Machines it may be useful to 
  [expand the disk to fill available space](../guest_specific_procedures/scientific_linux_cern_6.html)
* Ephemeral storage exists only for the life of a virtual machine instance, 
  it will persist across reboots of the guest operating system but when the instance
  is deleted so is the associated storage. The size of the ephemeral storage is 
  defined in the virtual machine flavor.
* Volumes are persistent virtualized block devices independent of any particular 
  instance. Volumes may be attached to a single instance at a time, but may be 
  detached or reattached to a different instance while retaining all data, much 
  like a USB drive. The size of the volume can be selected when it is created 
  within the quota limits for the particular project.