# Making your own images

The CERN private cloud offers the possibility of uploading your custom images to the Image service. The creation of these custom images requires a set of tools to produce and customize the underlying operating system.
In order to help with this task, a set of machines are provided called image builders. These servers contain the main set of tools that you would need in the process: creation, customization, generalization and upload tools to make it available in the cloud.
This sections explains how to use the image builders, guiding you throught the different steps of the image creation. Additionally, it is also explained how to deploy your own image builders in case you have specific requirements not met by the default configuration provided. Finally, we propose a list of tools that are worth to test in the context of image creation.

## Connecting to the builder

This section describes how to connect to one image builder and prepare the environment to successfully create new images.
Currently, we have the following machines available, with these convenient DNS aliases:

```
ozil-01.cern.ch
ozil-02.cern.ch
```

These nodes are accessible via ssh using a CERN account from the internal network, it is not possible to connect to the builders from outside CERN.

The first step is to connect to the machine and test that you have access to the libvirt. This connection requires that you have valid Kerberos credentials. This test can be done running the following commands:

```
$ ssh ozil-01
[lxbsq0920 ~]$ virsh -c qemu+tcp://ozil-01.cern.ch/system list
 Id    Name                         State
----------------------------------------------------
```

After checking that you have access and you are able to connect to the virtualization layer, it is time to configure the environment to produce your custom images.

The cloud team currently uses [Oz](https://github.com/clalancette/oz/wiki) to build the standard images for the CERN cloud (Windows, Scientific Linux). This is documented in the [OpenStack documentation](http://docs.openstack.org/image-guide/content/ch_creating_images_automatically.html) also.

Oz requires a configuration file to define the details of the VM that it will create on your behalf. 
This configuration file needs to be created per builder and should be created as follows exchanging 
LOGIN-NAME for the login name of your user and IMAGE-BUILDER-SERVER-NAME by the name of the host you are building on.

```
$ cat ~/.oz/oz.cfg
[paths]
output_dir = /var/tmp/LOGIN-NAME/oz/images
data_dir = /var/tmp/LOGIN-NAME/oz/data
screenshot_dir = /var/tmp/LOGIN-NAME/oz/screenshot

[libvirt]
uri=qemu+tcp://IMAGE-BUILDER-SERVER-NAME/system

[cache]
original_media = yes
modified_media = no
jeos = no
```

Run man oz-install for descriptions of the various options and make sure the paths actually exist. The image builders are configured to provide a big /var folder, so the recommended place to point your configuration file is /var/tmp/<username>.

## Building your custom image

This section guides you through the different steps in image building, it shows the basic scenarios you may face while creating you custom images.
Please note that the example Kickstart and TDL input files reside in the [cernops Github](http://github.com/cernops/openstack-image-tools). That repository also contains a helper script doit, which wraps around the necessary steps. A local copy of these scripts can be made into the openstack-image-tools directory using

```
$ git clone https://github.com/cernops/openstack-image-tools.git
```


The steps that ```doit``` executes are explained in detail in the following sections in case you prefer to run them manually or develop your own script. In case you want to use doit, you can execute it like in the following example. This does take serveral minutes to complete.

```
$ ./doit -c ~/.oz/oz.cfg slc6-server-x86_64.tdl    # doit assumes there is one slc6-server-x86_64.ks file in the same folder
```

doit will upload your created image to OpenStack, that means that you need to have your environment variables properly configure. Details are available in the  OpenStack Tools section. Alternatively, run the command recommended by the script to upload to Glance once you are authenticated.

```
$ glance image-create --name "SLC6 Server - x86_64 [2015-02-01]" --container-format bare --disk-format qcow2 --property hypervisor_type="qemu" --property os="LINUX" --file \/var\/tmp\/user\/oz\/images\/SLC6\ Server\ \-\ x86_64\.qcow2
```

## Create the image

The basic information you have to specify in Oz to create your image is a TDL and an auto installation file. The TDL file defines the details of the VM that will be launched (media location, disk size, files to inject,...), while the auto installation file is the configuration that will be applied during the installation and is OS dependent: Kickstart for RedHat, Unattend XML file for Windows,...

```
$ oz-install -d4 -u slc6-cern-server-x86_64.tdl -a slc6-cern-server-x86_64.ks -x slc6-cern-server-x86_64.xml -p -c ~/.oz/oz.cfg
```

## Convert the image

By default, Oz will create a raw disk image according to the size you specified in the TDL file. Even if OpenStack is able to manage this format, it's recommended to convert it to other formats that allow compression in order to reduce the size and reduce the time uploading the image to the cloud.
An interesting recipe to convert and compress the raw images created by Oz can be found [here](http://www.blog.turmair.de/2010/11/how-to-shrink-raw-qemu-kvm-images/). Only applying the last step already results in a huge gain:

```
$ qemu-img convert -c -O qcow2 /var/tmp/oz-user/images/images/slc6.dsk /var/tmp/oz-user/images/images/slc6.qcow2
$ ls -l /var/tmp/oz-user/images/images/slc6_x86_64.dsk /var/tmp/oz-user/images/images/slc6.qcow2
-rw-r--r--. 1 user user 5368709120 Jun 13 14:18 /var/tmp/oz-user/images/slc6.dsk
-rw-r--r--. 1 user user  557842432 Jun 13 14:20 /var/tmp/oz-user/images/slc6.qcow
```

In case you are creating a Windows machine, the supported format is VHD. Converting a raw disk to VHD is similar to the previous case:

```
$ qemu-img convert -c -O vpc /var/tmp/oz-user/images/images/windows.dsk /var/tmp/oz-user/images/windows.vhd
```

## Decontextualize the image

Before uploading the image to OpenStack, it is important to decontextualize the image and remove the installation log-files, network settings, etc.
On Linux instances, the recommended tool to do it is virt-sysprep. Virt-sysprep can reset or unconfigure a virtual machine so that clones can be made from it. Steps in this process include removing SSH host keys, removing persistent network MAC configuration, and removing user accounts. Virt-sysprep can also customize a virtual machine, for instance by adding SSH keys, users or logos. Each step can be enabled or disabled as required. Check the [man page](http://libguestfs.org/virt-sysprep.1.html) for details.

In case you are creating a Windows image, virt-sysprep doesn't support Windows guests yet. The recommended tool for this operating system is sysprep. It needs to be executed inside the running guest.

## Modify the image

If necessary, you can use use guestfish to edit your image. Typical examples would be where you take a standard image out of the glance repository and insert/delete/modify some files and then upload a new version under a different name.
Example session:

```
$ guestfish --rw -a /var/tmp/user/images/slc6.qcow2 -i

Welcome to guestfish, the libguestfs filesystem interactive shell for
editing virtual machine filesystems.

Type: 'help' for help on commands
      'man' to read the manual
      'quit' to quit the shell

Operating system: Scientific Linux CERN SLC release 6.3 (Carbon)
/dev/mapper/VolGroup00-LogVol00 mounted on /
/dev/vda1 mounted on /boot


<fs> vi /etc/cloud/cloud.cfg
<fs> exit
```

The utility comes from the libguestfs-tools-c RPM. Read the man page for details. More details can be found in the OpenStack documentation or the guestfish page in the libguestfs project.

## Upload the image

Use the glance image-create command to upload the image.

Example for SLC6:

```
$ glance image-create --name "My SLC6 image" --is-public False --container-format bare --disk-format qcow2 --property os=LINUX --property hypervisor_type=kvm --file var/lib/libvirt/images/slc6.qcow2
```

## Example for Windows:

```
$ glance image-create --name "My Windows image" --is-public False --container-format bare --disk-format vhd --property os=WINDOWS --property os_version=SERVER --property hypervisor_type=hyperv --file var/lib/libvirt/images/windows.vhd
```


**Note:** this assumes you have the OpenStack environment variables set. If you don't know how to do it, please take a look to the help page: OpenStack Tools.
Common tasks

## Monitoring your VM

It may happen that you run into troubles while creating your VM, or you are not getting the expect results in your images. The easiest option in this case is to connect through VNC to your instance and debug what is happening during the installation of your operating system.
The image builders are configured to only allow kerberos credentials connecting to libvirt, this restriction also applies to the VNC connections to the VM. Not all the VNC clients are able to do a VNC connection using kerberos credentials. However, the image builders are shipped with virt-viewer, one tool that offers you the option of viewing the console of your instance.

```
[~]$ ssh -X ozil-01
[lxbsq0920 ~]$ virsh -c qemu+tcp://ozil-01.cern.ch/system list --uuid
1cbf84ed-2ee0-4e8a-bb25-933309ff4cb3
[lxbsq0920 ~]$ virt-viewer -c qemu+tcp://ozil-01.cern.ch/system -- 1cbf84ed-2ee0-4e8a-bb25-933309ff4cb3
```

## Deploy your own image builder

The image builders maintained by the Cloud Service team are opened to the CERN users, but in case you need to deploy your own builders, here are the basic steps we have followed to configure them:

### Initial Linux installation

You can choose the Linux distribution that best suits your needs. In general, the only difference will be the name of the packages and the tools used to install them.
In RedHat based systems, you'll need at least the following packages:
```
libvirt
qemu-kvm
python-virtinst
oz
```

### Configure the network

Depending on the requirements of your use case, you may need network connectivity in your VM while installing. If this is the case, you have different options:
Use the default NAT network: you can use the default virbr0 network created by libvirt.

These networks are configured to do NAT with the main interface in the system.

* **Note 1:** when the network is created, new rules are added to iptables, this rules are important to ensure a proper functionality of the interface.
* **Note 2:** the previous tip is particularly useful if you run Puppet to configure your builders and you are using 'puppetlabs-firewall' module. Depending on your configuration, it can remove all the entries and keep only those created in Puppet (this means you need to put the iptables rules in your Puppet manifest).
* **Note 3:** the entry ```-A POSTROUTING -o virbr0 -p udp -m udp --dport 68 -j CHECKSUM --checksum-fill``` is particularly important if you're creating images based on SLC5 and other old systems.

Create a bridge: in case you need to give the machine an IP of the external network, you may find interesting creating a bridge to the main interface. This will provide external connectivity to the VM. You can find more information about how to do it [here](https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/s2-networkscripts-interfaces_network-bridge.html).
* **Note 1:** if you need external connectivity and your machine requires a particular MAC address, you can use -m flag provided by Oz.

### Configure non-root libvirt access (Kerberos)

The default libvirt configuration requires to have root permissions in the machine, raising some security concerns depending on your environment. If it's the case, and you want to provide non-root access to libvirt, these are the basic steps you have to follow to provide access based on Kerberos credentials:

Create the keytab files for the services:
/etc/libvirt/libvirt.keytab: this one will be used by Libvirt. File mode: 600 root:root.
/etc/vnc.keytab: it is used by qemu to provide Kerberos authentication in VNC connections. File mode: 600 qemu:root.

Change libvirt configuration:
```
/etc/libvirt/libvirtd.conf:
listen_tls = 0
listen_tcp = 1
auth_tcp = "sasl"
Change qemu configuration:
/etc/libvirt/qemu.conf:
vnc_listen = "0.0.0.0"
vnc_tls = 0
vnc_sasl = 1
Configure sasl2:
/etc/sasl2/libvirt.conf
mech_list: gssapi
keytab: /etc/libvirt/libvirt.keytab
/etc/sasl2/qemu-kvm.conf
mech_list: gssapi
keytab: /etc/vnc.keytab
auxprop_plugin: sasldb
Open firewall ports:
16509 TCP for libvirt.
5900-5999 TCP for VNC connections.
Configure libvirtd service:
/etc/sysconfi/libvirtd
LIBVIRTD_ARGS =--listen
```


Once you've made all the configuration changes, you have to restart the libvirtd service and the system will be able to be accessed using Kerberos credentials.

```
# service libvirtd restart
Stopping libvirtd daemon:                                  [  OK  ]
Starting libvirtd daemon:                                  [  OK  ]
```

## Tools

The previous sections described the process of image building base on the set of tools recommended by the Cloud Service. However, there are more tools freely available that are also worth to test. The following table list some of these tools:

| Tool | Description |
| ---- | ----------- |
| [Oz](https://github.com/clalancette/oz/wiki) | Oz is a tool for automatically installing operating systems into files with only minimal up-front input from the user. For each type of guest operating system, Oz supports up to three operations: operating system installation, operating system customization, and manifest generation.|
|[ImageFactory](https://github.com/redhat-imaging/imagefactory)|Image Factory enables appliance creation and deployment to multiple virtualization and Cloud providers.|
|[BoxGrinder](http://boxgrinder.org/)|BoxGrinder is a set of projects that help you grind out appliances for multiple virtualization and Cloud providers.|
|[Lorax](https://fedorahosted.org/lorax/)|Lorax is a tool to create Anaconda-based installation images|
|[virt-sysprep](http://libguestfs.org/virt-sysprep.1.html)|virt-sysprep can reset or unconfigure a virtual machine so that clones can be made from it. Steps in this process include removing SSH host keys, removing persistent network MAC configuration, and removing user accounts. Virt-sysprep can also customize a virtual machine, for instance by adding SSH keys, users or logos. Each step can be enabled or disabled as required.|
|[Guestfish](http://libguestfs.org/guestfish.1.html)|The guestfish tool can be used to edit images. Typical examples would be where you take a standard image out of the glance repository and insert/delete/modify some files and then upload a new version under a different name.|