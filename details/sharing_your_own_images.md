# Sharing your own images

Images can be shared between projects on the current release of OpenStack.
If an image had been uploaded to your currently active project, such as described in User Defined Images, you can then use the glance member-create operations to share that image with another project.

For example, to share an image, first source the project profile for the project containing the image you want to share and

```
$ . sourceproject.rc
$ glance image-create --name=ubuntu-quantal --disk-format=qcow2 --container-format=bare --property hypervisor_type=qemu --file quantal-server-cloudimg-amd64-disk1.img
$ glance image-show ubuntu-quantal
+----------------------------+--------------------------------------+
| Property                   | Value                                |
+----------------------------+--------------------------------------+
| Property 'hypervisor_type' | qemu                                 |
| checksum                   | 54c0658bb0408241e1f171edc316206f     |
| container_format           | bare                                 |
| created_at                 | 2013-12-18T07:18:47                  |
| deleted                    | False                                |
| disk_format                | qcow2                                |
| id                         | c258bb74-d950-4eda-9355-afcb53bfb61b |
| is_public                  | False                                |
...
```

You now need to find the id of the project you wish to share the image with. This will generally be done by looking at the openrc file that project and finding the OS_TENANT_ID variable (in this example, it is 65bf648d-8d07-48af-8c3a-fa162a5b283f)
Using this tenant ID as an argument on the command line of member-create, the image (c258bb74-d950-4eda-9355-afcb53bfb61b in this example) can then be shared.

```
. sourceproject.rc
glance image-show c258bb74-d950-4eda-9355-afcb53bfb61b # Check you can see the image

glance  member-create c258bb74-d950-4eda-9355-afcb53bfb61b 65bf648d-8d07-48af-8c3a-fa162a5b283f # share it```


glance member-list --image-id c258bb74-d950-4eda-9355-afcb53bfb61b # check it is shared
You should then have the image visible in the target project's glance list.

