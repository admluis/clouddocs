# Image properties

Images can have associated properties, which are key value pairs giving additional information
about the image such as what operating system is in the image or which hypervisor type is best
suited to run it.

## Properties available

Some of these are marked as CERN specific ones. Others are listed here as they have been fully tested on the CERN cloud infrastructure. A full list of the OpenStack image properties is available in the [OpenStack documentation](http://docs.openstack.org/cli-reference/content/chapter_cli-glance-property.html) although some options may not have been tested or configured in the CERN cloud.

These settings are made for all the centrally provided images for the CERN cloud. User supplied images or images from other sources may not provide all of the values.

| Property | Setting |
| -------- | ------- |
| hypervisor_type | <ul><li>hypervisor_type=qemu will be scheduled in a linux hypervisor running KVM</li><li>hypervisor_type=hyperv will be scheduled on a Windows hypervisor running hyperv</li></ul> Generally, Windows images are scheduled onto Hyper-V hypervisors and all other operating systems onto KVM. This is CERN specific |
| hw_watchdog_action | <ul><li>hw_watchdog_action=reset will cause the VM to be rebooted if it does not regularly reset the watchdog timer. For more information see ithe chapter on [availability techniques](http://clouddocs.web.cern.ch/clouddocs/availability_techniques/reboot_vm_on_failure.html)<li>hw_watchdog_action=disabled will not enable the watchdog. This is the default</ol>|
| os | Which family of operating systems is this image running <ul><li>LINUX<li>Windows</ul>|
| os_distro | <ul><li>os_distro=CC is for CERN CentOS 7</li><li>os_distro=SLC is for Scientific Linux CERN</li></ul><br> The other settings are as per the [OpenStack documentation](http://docs.openstack.org/cli-reference/content/chapter_cli-glance-property.html) such as Windows or rhel| 
| os_distro_major | The major distribution release number. Thus for CERN CentOS 7.1, this would be set to 7. For Windows releases, <ul><li>2008 is Windows Server 2008<li>2012 is Windows Server 2012<li>7 is Windows 7<li>8 is Windows 8</ul>|
| os_distro_minor | The minor distribution release number. Thus for CERN CentOS 7.1, this would be set to 1.|
| os_edition | For Windows images, <ul><li>Server means the image is a Windows Server relesae<li>Desktop is for the desktop OSes such as Windows 7<li>CERN Server means the customised version of the Server image including standard CERN components such as AFS</ul>|
| architecture | The architecture needed to run the image. <ul><li>x86_64 is 64 bit<li>i686 is 32 bit</ul>|
| release_date | The date when the image was made available for production. This is in the format YYYY-MM-DDTHH:MM:SS. It is different from the updated_at field in the image which is updated when a new version is provided to the image service. |
| upstream_provider | The URL to support provider. <ul><li>mailto:support@domain.com for email links </li><li>http://support.domain.com for website links</li></ul> |

## Using properties

Properties can be used to automatically select images based on criteria in scripts. For example

```
$ glance image-list --property os_distro=SLC --property os_distro_major=6 --property architecture=x86_64 --property os_edition=Server --sort-key updated
+--------------------------------------+-----------------------------------+-------------+------------------+-----------+--------+
| ID                                   | Name                              | Disk Format | Container Format | Size      | Status |
+--------------------------------------+-----------------------------------+-------------+------------------+-----------+--------+
| 9c3d022c-f127-44bb-a732-1336e4f971ce | SLC6 Server - x86_64 [2014-11-06] | qcow2       | bare             | 817102848 | active |
| 321b8583-967f-4f56-913e-2a10e058ff37 | SLC6 Server - x86_64 [2014-01-30] | qcow2       | bare             | 789708800 | active |
| b8018173-fdfc-442c-9337-612fc702652a | SLC6 Server - x86_64 [130624]     | qcow2       | bare             | 733151232 | active |
| 78deafa9-93a7-41d9-9afb-8c62e29e4259 | SLC6 Server - x86_64 [130920]     | qcow2       | bare             | 702545920 | active |
| d1cb4dce-7a03-4342-a6c1-9677ecb8770d | SLC6 Server - x86_64 [2014-08-05] | qcow2       | bare             | 801112064 | active |
+--------------------------------------+-----------------------------------+-------------+------------------+-----------+--------+
```

Thus, the latest SLC 6 Server image can be obtained as follows

```
$ glance -k image-list --property os_distro=SLC --property os_distro_major=6 --property architecture=x86_64 --property os_edition=Server --sort-key updated_at | awk 'NR==4 { print $2 }'
```