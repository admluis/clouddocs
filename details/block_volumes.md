# Creating block volumes

**Note: **The volume capability is currently not available for Windows guests. Studies are underway to add this capability.

OpenStack provides two classes of block storage, ephemeral storage and persistent volumes. Ephemeral storage exists only for the life of an instance, it will persist across reboots of the guest operating system but when the instance is deleted so is the associated storage. The size of the Ephemeral storage is defined in the flavor of the virtual machine and is fixed for all virtual machines of that particular flavor. The service level for ephemeral storage is dependent on the underlying hardware.

Volumes are persistent virtualized block devices independent of any particular instance. Volumes may be attached to a single instance at a time (i.e. not like a distributed filesystem such as AFS or DFS), but they may be detached or reattached to a different instance while retaining all data, much like a USB drive. Volumes are stored on redundant storage and are therefore more resilient against hardware failures.

You can define additional volumes to be attached to a virtual machine in addition to the system disk. This is useful if you need more space than is provided on the initial flavor and can also allow some interesting flexiblity for snapshotting or moving volumes around between virtual machines.

The steps are
1. Define a disk volume of the size required (10GB in example below) with the volume name defined (testvol in the example below)
2. Check that the volume is available for mounting using ```openstack volume list```
3. Attach that volume to the instance that you would like it to be available on
4. Log in to the instance and format/mount as required

For example, to define a volume of 10GB named timvol143, attach it to the VM test143 and fornat/mount it as ext4, you do the following on a machine which has the unified openstack command line tool installed.

The volume type describes the type of storage that is being requested.

The following storage types are currently available. The standard type is allocated for projects by default.

| Storage Type | Description | Maximum Performance |
| ------------ | ----------- | ------------------- |
| standard | The 'standard' volume type provides a reliable store which is tolerant to at least one disk failure without user impact or data loss. This is the default. 'standard' volumes can only be attached to hosts running on KVM (i.e. not Windows servers) | 80MB/s and 100 IO operations (both, read and write)|
| io1 | The 'io1' volume type provides the same reliability level as 'standard' volumes, but is meant for applications with higher IO demands. Access to the 'io1' volume type is only granted on a request/approval process. 'io1' volumes can only be attached to hosts running on KVM (i.e. not Windows servers) | 120MB/s and 500 IO operations (both, read and write)|
| cp1 | The 'cp1' volume type provides the same performance level as 'standard' volumes, but is meant for applications with higher reliability demands: cp1 volumes are hosted on critical power which guarantees availability even in the event of a power cut. Access to the 'cp1' volume type is only granted on a request/approval process. 'cp1' volumes can only be attached to hosts running on KVM (i.e. not Windows servers) | 80MB/s and 100 IO operations (both, read and write)|
| cp2 | The 'cp2' volume type provides the same performance level as 'standard' volumes, but is meant for applications with higher reliability demands: cp2 volumes are hosted on critical power which guarantees availability even in the event of a power cut. Access to the 'cp2' volume type is only granted on a request/approval process. 'cp2' volumes are meant to be attached to virtual machines running Windows) | 80MB/s and 100 IO operations (both, read and write)|
## Volume creation

```
$ openstack volume create --description "Volume for documentation" --size 10 timvol143
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| attachments         | []                                   |
| availability_zone   | nova                                 |
| bootable            | false                                |
| created_at          | 2014-12-18T13:27:27.754856           |
| display_description | Volume for Documentation             |
| display_name        | timvol143                            |
| encrypted           | False                                |
| id                  | d8d8be88-f1a4-4120-b1c2-50621a981c74 |
| properties          |                                      |
| size                | 10                                   |
| snapshot_id         | None                                 |
| source_volid        | None                                 |
| status              | creating                             |
| type                | standard                             |
+---------------------+--------------------------------------+
```

The size is measured in GB.

The status reported will become available when the volume has been created. This status can be checked using ```openstack volume show```.

## Attaching a volume

Once the volume has been created, it can be attached to the virtual machine.

```
$ openstack server add volume tim-centos7-143 timvol143
```

The arguments are

   * tim-centos7-143 is the virtual machine where you would like to attach the image. This is as previously created using ```openstack server create```.
   * timvol143 is the volume id from ```openstack volume list``` as created above

**Note:** An optional device argument can also be /dev/vdX but this does not seem to work currently. For Linux, the actual device allocated in the guest VM is the next free one rather than the one specified.

The operation can be checked by seeing if this is now shown in the ```openstack server list``` command.
Log into the VM to check and format the disk:

```
$ ssh root@tim-centos7-143
# grep vdb /proc/partitions
 252       48   12582912 vdb
 ```

**Note:** For SLC 5 systems, it is necessary to reboot in order to have the new device visible. SLC 6 and CC 7 systems will show it without a reboot.

Create a file system on it and mount it. Using a volume label is useful in the future to avoid needing to find the exact device name as you attach different volumes. Using a label, such as testvol in the example below, which is the same as the name in OpenStack also makes tracking easier when volumes are not mounted. The filesystem type can be one supported by the Linux distribution but xfs and ext4 are the most widely used.

```
# mkfs -t ext4 -L testvol /dev/vdb
# mount -L testvol /mnt
# df
```

The filesystem should be added to /etc/fstab if the desired behaviour is to mount automatically on restart. Using the LABEL parameter here will ensure the correct volume is chosen if multiple volumes are attached.

```
LABEL=testvol /mnt ext4 noatime,nodiratime,nobarrier,user_xattr    0       0
```

## Removing disk volumes

The steps to properly remove disk volumes are:

   * Umount the volume on the virtual machine
   * Detach it from the virtual machine
   * Delete the volume
   * Logged into the virtual machine that has the volume mounted, unmount it first

```
# umount /mnt
```

**Note:** detaching a volume while it is being mounted is possible, but the next access on the VM to that volume will return I/O errors.
Then use the openstack command line interface to detach the volume from the virtual machine:

```
$ openstack server remove volume tim-centos7-143 timvol143
```

where tim-centos7-143 is the virtual machine and the second parameter, timvol143, is the volume as shown in ```openstack volume list```.

**Note:** For SLC 5 systems, it is necessary to reboot in order to have the device detached properly and prevent data corruptions. SLC 6 and CC 7 systems don't require an additional reboot.

Check that the volume is in state 'available' again.

If that's the case, the volume is now ready to either be attached to another virtual machine or, if it is not needed any longer, to be completely removed (note that this step cannot be reverted!):

```
$ openstack volume delete timvol143
```

Your volume will now go into state 'deleting' and completely disappear from the ```openstack volume list``` output

## Common Error Messages

From the ```openstack volume``` command, the following errors can be received

| Error | Cause |
| ----- | ----- |
|VolumeSizeExceedsAvailableQuota: Requested volume or snapshot exceeds allowed Gigabytes quota|Your total space for the volume service exceeds the allowance for the project. If you require further quota, please contact the helpdesk. Please note that additional quota is only granted for shared projects. Additional personal project quota requests will not be accepted.|
|VolumeLimitExceeded: Maximum number of volumes allowed (N) exceeded|You have exceeded the number of volumes. Please contact the helpdesk to request to raise this limit for a project. Additional personal project quota requests will not be accepted.|
