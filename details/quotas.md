# Quotas and Accounting

## Quotas

Each project is assigned a quota which defines how much resources that a project can use. When resource consuming operations such as virtual machine creation are performed, the request is validated against the maximum quota permitted for the current project (as set by the environment variables or Horizon dashboard).

The project quota can be checked using 

```
$ openstack quota show 841615a3-ece9-4622-9fa0-fdc178ed34f8
+----------------------+--------------------------------------+
| Field                | Value                                |
+----------------------+--------------------------------------+
| cores                | 20                                   |
| fixed-ips            | -1                                   |
| floating_ips         | 0                                    |
| gigabytes            | 100                                  |
| gigabytes_standard   | -1                                   |
| injected-file-size   | 10240                                |
| injected-files       | 5                                    |
| injected-path-size   | 255                                  |
| instances            | 10                                   |
| key-pairs            | 100                                  |
| project              | 841615a3-ece9-4622-9fa0-fdc178ed34f8 |
| properties           | 128                                  |
| ram                  | 40960                                |
| security_group_rules | 0                                    |
| security_groups      | 0                                    |
| snapshots            | 10                                   |
| snapshots_standard   | -1                                   |
| volumes              | 10                                   |
| volumes_standard     | -1                                   |
+----------------------+--------------------------------------+```
```

| Quota Name | Description |
| ---------- | ----------- |
|cores|Number of instance cores (VCPUs) allowed per project.|
|fixed-ips|Number of fixed IP addresses allowed per project. This number must be equal to or greater than the number of allowed instances. A value of -1 means unlimited|
|floating_ips|Number of floating IP addresses allowed per project. A value of -1 means unlimited. See [Restrictions](../additional/current_restrictions.md) for the current status of floating IPs in the CERN cloud|
|gigabytes|Total space in external [volumes](../details/volumes.md)|
|gigabytes_*type*|Total space in the volume storage class *type* such as standard|
|injected-file-size|Number of content bytes allowed per injected file.|
|injected-path-size|Number of bytes allowed per injected file path.|
|injected-files|Number of injected files allowed per project.|
|instances|Number of virtual machines allowed per project.|
|key-pairs|Number of key pairs allowed per user.|
|ram|Total megabytes of instance ram allowed per project.|
|security-groups|Number of security groups per project.|
|security-group-rules|Number of rules per security group.|
|snapshots|Total number of volume snapshots allowed per project|
|snapshots_*type*|Snapshots allowed for the volume storage class *type*|
|volumes|Total volumes allowed for the project|
|volumes_*type*|Volumes allowed for the volume storage class *type*|