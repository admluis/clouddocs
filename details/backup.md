# Backup and snapshots

The virtual machine systems disks backups are covered in [Using OpenStack](http://clouddocs.web.cern.ch/clouddocs/using_openstack/backups.html). This section covers backup and recovery of volumes allocated external to the virtual machine system disks.

As mentioned in the system disk backup section, there is no default backup performed for the cloud. Data can be stored on one of the IT reliable data stores such as 

  * [Database-on-Demand](http://information-technology.web.cern.ch/services/database-on-demand)
  * Oracle
  * [AFS](http://information-technology.web.cern.ch/services/afs-service)
  * DFS

However, if you wish to use an external volume attached to a virtual machine and have that data backed up, there are a number of possible approaches.

**Note:** Do not use the ```openstack backup create``` command. This has not been configured for the CERN OpenStack cloud and will produce the error "Service cinder-backup could not be found"

## Locally installed backup client

To use a local backup client (as described in [Backups and snapshots](http://clouddocs.web.cern.ch/clouddocs/using_openstack/backups.html)), the volume would need to be mounted on the virtual machine where the client is installed.

The include/exclude lists for filesystems and files can then be adjusted to include the newly mounted file system. This is especially useful where you want to be able to restore individual files.

## Rollback & rebuild

Typical cases for rollback/rebuild are test environments. A reference volume is created at the start of a loop of tests, each test resets to its initial state and then rolls back when the test is completed for the next test to run.

This can be performed as follows:

  * A master volume is created as described in [Create volumes](http://clouddocs.web.cern.ch/clouddocs/details/block_volumes.html)
  * A clone volume is created using the master contents as a source
  * Work is done using the volume, such as running a test
  * The clone volume is deleted and re-created from the master contents

The state of the VM before the operation is as follows

  * A volume *timmaster143* has been created
  * It has been mounted on */mnt/master* on the VM and its contents initialised
  * It has been unmounted and is available in ```openstack volume list```

On the client (such as lxplus), detach the volume, clone it and attach it

```
$ openstack server remove volume timhost143 timmaster143
$ openstack volume create --source timmaster143 timclone143
$ openstack server add volume timhost143 timclone143```

The tests can then be run by re-mounting the clone volume and working with it. Once the tests are completed, the 
volume can be detached from the host using ```openstack server remove```, deleted and re-created from the master.

### Errors

Invalid volume: must be available

## Snapshots for testing and cloning

The typical use case for testing and cloning is a production service where there is a significant change, such as new application
software to be validated. This assumes that the application executables and data are stored on an external volume. The steps that would be needed would be

  * Create a new VM for the test environment (the approach outlined in [Backups and snapshots](http://clouddocs.web.cern.ch/clouddocs/using_openstack/backups.html) could be used or a new virtual machine created with configuration management tools such as Puppet)
  * Create a snapshot of the existing application volume
  * Create a volume from the snapshot and attach it to the test VM
  * Run the tests
  * Delete the test VM and volume on completion

The snapshotting steps are as follows assuming a master volume *arnemaster2911*.

```
$ openstack snapshot create --name arnesnap2911 arnemaster2911
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| created_at          | 2015-02-13T15:14:28.762428           |
| display_description | None                                 |
| display_name        | arnesnap2911                         |
| id                  | ae8de576-3e56-4be1-bfe4-1deb4e278ee2 |
| metadata            | {}                                   |
| size                | 1                                    |
| status              | creating                             |
| volume_id           | b3f43655-aa0f-42ed-91b6-23952221b0aa |
+---------------------+--------------------------------------+

$ openstack snapshot list
+--------------------------------------+--------------+---------------------+-----------+------+
| ID                                   | Display Name | Display Description |Status    | Size |
+--------------------------------------+--------------+---------------------+-----------+------+
| ae8de576-3e56-4be1-bfe4-1deb4e278ee2 | arnesnap2911 | None                |available |    1 |
+--------------------------------------+--------------+---------------------+-----------+------+

$ openstack volume create --snapshot-id ae8de576-3e56-4be1-bfe4-1deb4e278ee2 --size 1 arneclone2911
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| attachments         | []                                   |
| availability_zone   | nova                                 |
| bootable            | false                                |
| created_at          | 2015-02-13T15:16:22.373573           |
| display_description | None                                 |
| display_name        | arneclone2911                        |
| encrypted           | False                                |
| id                  | 68591617-93da-4e48-959d-fb5db556b1bf |
| properties          |                                      |
| size                | 1                                    |
| snapshot_id         | ae8de576-3e56-4be1-bfe4-1deb4e278ee2 |
| source_volid        | None                                 |
| status              | creating                             |
| type                | standard                             |
+---------------------+--------------------------------------+

$ openstack volume list
+--------------------------------------+----------------+-----------+------+----------+
| ID                                   | Display Name   | Status    | Size |Attached |
+--------------------------------------+----------------+-----------+------+----------+
| 68591617-93da-4e48-959d-fb5db556b1bf | arneclone2911  | available |    1 |
|
| b3f43655-aa0f-42ed-91b6-23952221b0aa | arnemaster2911 | available |    1 |
|
| c9c3469f-0635-4314-a4fb-588409c625ce | None           | available |    1 |
|
| 81dcbc28-a23e-488e-86db-0a0a6e167e2e | None           | in-use    |   32 |
|
+--------------------------------------+----------------+-----------+------+----------+```

The newly created volume arneclone2911 can now be attached to the test VM.

Note: While OpenStack can be forced to snapshot an attached volume, it is recommended
to detach the master volume from the master VM before creating the snapshot in order
to guarantee consistency.

Note: In order to delete a clone volume the corresponding snapshot needs to be
deleted first.

## Snapshots to move between projects and clouds

## Snapshots for archive






