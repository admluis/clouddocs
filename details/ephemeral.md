# Ephemeral

Ephemeral disks can be useful where you want to partition the second disk or have a specific disk configuration which is not possible within the system disk configuration.

Ephemeral storage is defined by the virtual machine flavor when the VM is created.

```
$ openstack flavor list
+----+--------------+-----------+------+-----------+------+-------+-------------+-----------+-------------+
| ID | Name         | Memory_MB | Disk | Ephemeral | Swap | VCPUs | RXTX_Factor | Is_Public | extra_specs |
+----+--------------+-----------+------+-----------+------+-------+-------------+-----------+-------------+
| 1  | m1.tiny      | 512       | 0    | 0         |      | 1     | 1.0         | True      | {}          |
| 11 | sft.medium   | 8192      | 20   | 40        |      | 4     | 1.0         | True      | {}          |
```

In this case, the sft.medium flavor has an ephemeral disk of 40GB.

When an instance with ephemeral storage is created, a second disk is defined and mounted automatically on /dev/vdb (by default).

```
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/mapper/VolGroup00-LogVol00
                       8547036   1691360   6421500  21% /
tmpfs                  4095860         0   4095860   0% /dev/shm
/dev/vda1               198337     28571    159526  16% /boot
/dev/vdb              41284928    180236  39007540   1% /mnt```