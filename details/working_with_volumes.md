# Working with volumes
## Extending volumes
A volume can be made larger while maintaining the existing contents, assuming the file system supports resizing.

**Note:** The volume needs to be unmounted currently for this operation to be performed.

**Note:** The procedure has been tested with ext4 and XFS filesystems only.

The steps are as follows

  * Unmount the filesystem in the VM
  * Detach the volume from the guest VM
  * Extend the volume to its new size
  * Attach the volume to the guest VM
  * Mount the filesystem on the vM
  * Extend the volume to its new size

**Note:** The extend operation is currently only possible using the cinder command line tool. The functionality will be added to the openstack client (OSC) in future. This means you have to source the password version of the openrc profile rather than use the Kerberos one.

On the client machine (such as lxplus), the steps for the host tim-centos7-143 and the volume timvol143 (volume id of d8d8be88-f1a4-4120-b1c2-50621a981c74) are as follows to extend the volume to 25 GB.

```
$ nova volume-detach tim-centos7-143 d8d8be88-f1a4-4120-b1c2-50621a981c74
$ cinder extend d8d8be88-f1a4-4120-b1c2-50621a981c74 25
$ nova volume-attach tim-centos7-143 d8d8be88-f1a4-4120-b1c2-50621a981c74
```

Then on the guest,

```
# mount /dev/vdc /mnt/timvol143
# resize2fs /dev/vdc
resize2fs 1.42.9 (28-Dec-2013)
Filesystem at /dev/vdc is mounted on /mnt/clone; on-line resizing required
old_desc_blocks = 2, new_desc_blocks = 4
The filesystem on /dev/vdc is now 6553600 blocks long.
```

For XFS, a similar procedure can be followed using the ```xfs_grow``` command.

### Common Errors

| Message | Explanation |
| ------- | ----------- |
| ERROR: Invalid volume: Volume status must be available to extend. | The volume must currently be detached in order to be extended. |
| ERROR: Invalid input received: New size for extend must be greater than current size. | Volumes cannot be shrunk, only expanded. A new filesystem could be created and the contents copied over. |

## Transferring volumes from one project to another one
A volume can be transferred from one OpenStack project to another one by means
of the 'cinder transfer*' functions.

The steps are as follows

  * In the source project, create a volume transfer; this will also give you an authorization key
  * In the target project, accept the transfer using the authorization key

For example, in the source project run:

```
$ cinder transfer-create e20ed740-d4cb-4740-93c8-4996dc90ec1b
+------------+--------------------------------------+
|  Property  |                Value                 |
+------------+--------------------------------------+
|  auth_key  |           a4bcc242682xxxxx           |
| created_at |      2015-01-22T13:33:27.092622      |
|     id     | 80b79e64-315c-410e-934e-d58cc8cafbf7 |
|    name    |                 None                 |
| volume_id  | e20ed740-d4cb-4740-93c8-4996dc90ec1b |
+------------+--------------------------------------+

$ cinder transfer-list
+--------------------------------------+--------------------------------------+------+
|                  ID                  |              Volume ID               | Name |
+--------------------------------------+--------------------------------------+------+
| 80b79e64-315c-410e-934e-d58cc8cafbf7 | e20ed740-d4cb-4740-93c8-4996dc90ec1b | None |
+--------------------------------------+--------------------------------------+------+
```

Then, after changing into the target project, accept the transfer

```
$ cinder transfer-accept 80b79e64-315c-410e-934e-d58cc8cafbf7 a4bcc242682xxxxx
+-----------+--------------------------------------+
|  Property |                Value                 |
+-----------+--------------------------------------+
|     id    | 80b79e64-315c-410e-934e-d58cc8cafbf7 |
|    name   |                 None                 |
| volume_id | e20ed740-d4cb-4740-93c8-4996dc90ec1b |
+-----------+--------------------------------------+
```

```cinder list``` in the target project will now show the volume.