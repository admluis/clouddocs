# Images

OpenStack images provide the source for booting a virtual machine. An image consists of an operating system, some optional additional packages and some flags to help OpenStack place these on the right hypervisor.

For more detailed information about OpenStack's image management, the [OpenStack image creation documentation](http://docs.openstack.org/trunk/openstack-image/content/) provides further references and links.

