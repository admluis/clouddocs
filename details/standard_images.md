# Standard images

The CERN cloud includes a set of centrally maintained images for supported operating systems. These are regularly updated with the latest security patches and operating system updates. If there are issues booting these virtual machines, support tickets can be raised.

These images (both Windows and Linux) include cloud-init to perform contextualisation based on the user data specified when the VM are instantiated. Contextualisation allows the virtual machine to be configured further on the first boot such as to install additional packages or call a configuration management system such as Puppet. Further details can be found in the chapter on [Contextualisation](http://clouddocs.web.cern.ch/clouddocs/using_openstack/contextualisation.html).

The IT supplied images will be updated every few months to include the latest patches. The date in the form of [YYYY-MM-DD] is added to the end of the name to show the version of the image.

These images are public, and visible to all users. The following standard configurations are provided:

## CERN CentOS

The following base image of the [CERN CentOS 7 distribution](http://linux.web.cern.ch/linux/centos7/) is available:

  * CC7 64 bit

There are two variants of this image:

  * "Extra": the VM will be be configured with AFS, Kerberos, user accounts, automatic updates, etc.
  * "Base": basic server configuration, to be used as the basis for further configurations (i.e. Puppet-managed VMs). Note: automatic updates are not configured in this image.

**Note:** No CentOS 7 32 bit version is available as this is not supported by the upstream project.

## Scientific Linux CERN

The following base images of [Scientific Linux CERN 5](http://linux.web.cern.ch/linux/scientific5/) and [Scientific Linux CERN 6](http://linux.web.cern.ch/linux/scientific6/) are available:

   * SLC6 64 bit
   * SLC6 32 bit
   * SLC5 64 bit
   * SLC5 32 bit

These images come in two different variants:

   * "CERN Server": the VM will be be configured with AFS, Kerberos, user accounts, automatic updates, etc.
   * "Server": basic server configuration, to be used as the basis for further configurations (i.e. Puppet-managed VMs). Note: automatic updates are not configured in this image.

**Note:** New applications are recommended to not use Scientific Linux CERN 5 as this in maintenance mode.

## Windows images

The Windows images also include cloud-init support for Windows.

   * Window 7 Desktop 64 bit
   * Windows 2008 R2 SP1 Server 64 bit
   * Windows 2012 R2 64 bit

