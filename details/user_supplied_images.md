# User supplied images

Along with the centrally supported images, there are many sources of images around the Internet.

## Procedure for uploading images

Along with the standard images, it is possible to create your own images. In many cases, it is better to use a standard image and a configuration management system such as Puppet to make the customisations through contextualisation.

However, in some cases, a custom image may be more efficient although regular maintenance to ensure patches are kept up to date is required.

Image sizes are limited to 25GB.

**Note:** The Cloud Infrastructure service does not support any images other than those listed previously. If there are issues using other images, support should be requested from the image provider rather than from the cloud infrastructure support team. This may also apply to higher level services such as configuration management and monitoring thus contextualisation should be considered if the operating system is one of the standard configurations listed above.

**Note:** Users with private images should keep a copy of the image they have uploaded in their private archives. If an image is accidentally deleted or corrupted, the user should upload a new copy as no archive is maintained within the cloud.

**Note:** Data inside images can be accessed by members of the project. Thus, passwords should not be stored in the images and security information should be passed as part of contextualisation.

Project members can upload their own images to their projects. These images are private, meaning that they are only available to the users of the projects they are uploaded for.

Many open source projects such as Ubuntu and Fedora now produce pre-build images which can be used for certain clouds.

Building your own images is also possible but support for these images and the virtual machines created from them should be via the image provider, such as the community mailing lists on the Internet rather than the formal CERN IT support lines. Details on tools that have been used is available in the chapter [Building your own images](http://clouddocs.web.cern.ch/clouddocs/details/making_your_own_images.html).

As for all resources in the cloud, the owner of the instance is responsible that

  * There is automatic software updating enabled on a regular basis
  * Access to and usage of the instance is limited according to the [CERN Computing Rules](https://security.web.cern.ch/security/rules/en/index.shtml)

If you create your own images, please ensure that

  * There is automatic software updating on a regular basis to ensure security updates
  * A time synchronisation process such as NTP is enabled

As an example of how images can be uploaded, the following steps are provided.

Taking an example of the Ubuntu image, the steps to upload such an image (using the command line tools available on lxplus) are as follows

```
wget http://uec-images.ubuntu.com/quantal/current/quantal-server-cloudimg-amd64-disk1.img
```

Having retrieved the image and authenticated to OpenStack using the openrc source, it can be uploaded to Glance as a private image (i.e. only available in the project you are authenticated for)

If not otherwise specified, it will default to LINUX image.

```
$ openstack image create "Ubuntu Quantal" \
      --disk-format=qcow2 --container-format=bare \
      --property hypervisor-type=qemu \
      --file quantal-server-cloudimg-amd64-disk1.img 
+------------------+--------------------------------------+
| Field            | Value                                |
+------------------+--------------------------------------+
| checksum         | b4ed62b0423ab57fd1305fcba2cb466c     |
| container_format | bare                                 |
| created_at       | 2014-12-03T12:27:01                  |
| deleted          | False                                |
| deleted_at       | None                                 |
| disk_format      | qcow2                                |
| id               | 65dc3e3f-d649-4c7b-aed0-d119eb368abd |
| is_public        | False                                |
| min_disk         | 0                                    |
| min_ram          | 0                                    |
| name             | Ubuntu Quantal                       |
| owner            | 841615a3-ece9-4622-9fa0-fdc178ed34f8 |
| properties       | {u'hypervisor_type': u'qemu'}        |
| protected        | False                                |
| size             | 224199168                            |
| status           | active                               |
| updated_at       | 2014-12-03T12:27:08                  |
| virtual_size     | None                                 |
+------------------+--------------------------------------+
```

This can then be checked using ```openstack image list```.

Once loaded, the image can then be used to create a virtual machine. The my-key key-name should be replaced by the apropriate one for the project concerned.

```
$ openstack server create --key-name lxplus --flavor m1.small --image ubuntu-quantal ubtest4
+------------------------+--------------------------------------+
| Property               | Value                                |
+------------------------+--------------------------------------+
| OS-DCF:diskConfig      | MANUAL                               |
| OS-EXT-STS:power_state | 0                                    |
| OS-EXT-STS:task_state  | scheduling                           |
| OS-EXT-STS:vm_state    | building                             |
| accessIPv4             |                                      |
| accessIPv6             |                                      |
| config_drive           |                                      |
| created                | 2013-03-23T18:39:08Z                 |
| flavor                 | m1.small                             |
| hostId                 |                                      |
| id                     | 00135d38-b646-465c-9fc9-a0ab541d11fd |
| image                  | ubuntu-quantal                       |
| key_name               | lxplus                               |
| metadata               | {}                                   |
| name                   | ubtest4                              |
| progress               | 0                                    |
| security_groups        | [{u'name': u'default'}]              |
| status                 | BUILD                                |
| tenant_id              | c7f08856cf7640a5a79fee4a8ef4f686     |
| updated                | 2013-03-23T18:39:08Z                 |
| user_id                | timbell                              |
+------------------------+--------------------------------------+
```

The machine can then be accessed using ssh as root. If you used a key other than your lxplus ssh key, you should add -i <keyname>

```
ssh root@ubtest4.cern.ch
```

Many of these images include the cloud-init package for contextualisation. This allows easy customisation of the image after installation. See the [contextualisation chapter](http://clouddocs.web.cern.ch/clouddocs/using_openstack/contextualisation.html) for more details on how to use this feature.

Similar techniques can be applied to the other images in the list.

The image can be tagged with a number of different properties. The properties used at CERN are covered in [Image Properties](http://clouddocs.web.cern.ch/clouddocs/details/image_properties.html). Full details are available in the [OpenStack documentation](http://docs.openstack.org/cli-reference/content/chapter_cli-glance-property.html). It is recommended that the following properties be set

  * os
  * os_distro
  * release_date

In addition, the following options can have a performance impact

  * hw_rng_model

## Creating an Image from an ISO

Some system software is delivered in ISO image format. The following describes the steps to upload an ISO image to Glance in order to be able to instantiate a virtual machine. Depending on the image contents, manual contextualisation and security login through the console may be required in order to get a usable environment.
For example, these steps show how to create an image from the Freedos ISO available at http://www.freedos.org/download/ and downloaded to fd11src.iso.

```
$ openstack image create "freedos11" \
      --disk-format=iso --container-format=bare \
      --property hypervisor-type=qemu --file fd11src.iso
+------------------+--------------------------------------+
| Property         | Value                                |
+------------------+--------------------------------------+
| checksum         | 2e0ab23bec79ff33071d80ea26f124dc     |
| container_format | bare                                 |
| created_at       | 2014-01-07T07:12:29                  |
| deleted          | False                                |
| deleted_at       | None                                 |
| disk_format      | iso                                  |
| id               | f826df03-e223-48fc-b357-5f950c9f5f43 |
| is_public        | False                                |
| min_disk         | 0                                    |
| min_ram          | 0                                    |
| name             | freedos11                            |
| owner            | 841615a3-ece9-4622-9fa0-fdc178ed34f8 |
| protected        | False                                |
| size             | 40828928                             |
| status           | active                               |
| updated_at       | 2014-01-07T07:12:33                  |
+------------------+--------------------------------------+
```


### Error Messages
| Error | Description |
| ----- | ----------- |
| Denying attempt to upload image larger than 2147483648 bytes.(HTTP 400)|The maximum image size for users to upload has been exceeded. Re-build the image to be a smaller size with techniques such as ephemeral disks or external volumes|
|403 Forbidden.Access was denied to this resource. (HTTP 403)|Only administrators can set images to be public. The --public option should not be used by non-administrators. To share images between projects, use the [image sharing](http://clouddocs.web.cern.ch/clouddocs/details/sharing_your_own_images.html) functions|

## Image Deletion

**Note:** Images should only be deleted if no VM is running which was booted from that image.

Images can be deleted using the ```openstack image delete``` command.

```
openstack image delete BOSH-b1d714e1-2b02-4ab4-ad5e-a9a4a253484e
```


## Sources of images

**Note:** Support for images other than the [standard ones](http://clouddocs.web.cern.ch/clouddocs/details/standard_images.html) should be obtained from the provider of the image rather than the CERN cloud support.

**Note:** Only use images from a reputable source as they could contain security issues.

| Operating System | Link and descriptions |
| ---------------- | --------------------- |
| CernVM 3 | The CernVM team provide images that can be shared with user projects. Please see [Running CernVM on CERN OpenStack](http://cernvm.cern.ch/portal/openstack) for details.|
| Ubuntu | [Image repository](http://uec-images.ubuntu.com/) |
| Fedora | [Image repository](http://berrange.fedorapeople.org/images)


## Additional Information

The OpenStack [documentation](http://docs.openstack.org/image-guide/content/) contains more details of how to work with images. Some features may not be configured on the CERN cloud.