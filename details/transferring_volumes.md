# Transferring volumes

Volumes are specific to a project and can only be attached to one virtual machine at a time.

There are mechanisms to transfer volumes between projects as detailed in the following steps.

The steps neeed to be performed as follows

  * The volume is detached from the VM in the source project and is available
  * The administrator of the source project marks the volume as being ready for transfer. This process generates an authentication key.
  * The authentication key has to be transmitted via a secure channel to the administrator of the destination project
  * The administrator of the desstination project acccepts the transfer using the authentication key.
  * The volume is attached to the VM in the destination project

Authenticating against the source project, create the transfer request

```
$ cinder transfer-create e20ed740-d4cb-4740-93c8-4996dc90ec1b
+------------+--------------------------------------+
|  Property  |                Value                 |
+------------+--------------------------------------+
|  auth_key  |           a4bcc242682ce689           |
| created_at |      2015-01-22T13:33:27.092622      |
|     id     | 80b79e64-315c-410e-934e-d58cc8cafbf7 |
|    name    |                 None                 |
| volume_id  | e20ed740-d4cb-4740-93c8-4996dc90ec1b |
+------------+--------------------------------------+```

The volume can be checked as in the transfer status using ```cinder transfer-list``` as follows and the volume is in status awaiting-transfer.

```
$ cinder transfer-list
+--------------------------------------+--------------------------------------+------+
|                  ID                  |              Volume ID               | Name |
+--------------------------------------+--------------------------------------+------+
| 80b79e64-315c-410e-934e-d58cc8cafbf7 | e20ed740-d4cb-4740-93c8-4996dc90ec1b | None |
+--------------------------------------+--------------------------------------+------+```

```
+--------------------------------------+-------------------+--------------+------+-------------+----------+--------------------------------------+
| e20ed740-d4cb-4740-93c8-4996dc90ec1b | awaiting-transfer |     None     |  1   |   standard  |  false   |                                      |
+--------------------------------------+-------------------+--------------+------+-------------+----------+--------------------------------------+```
 

The administrator of the destination project should authenticate and receive the authentication key reported above. The transfer can then be initiated.
 
```
$ cinder transfer-accept 80b79e64-315c-410e-934e-d58cc8cafbf7 a4bcc242682ce689
+-----------+--------------------------------------+
|  Property |                Value                 |
+-----------+--------------------------------------+
|     id    | 80b79e64-315c-410e-934e-d58cc8cafbf7 |
|    name   |                 None                 |
| volume_id | e20ed740-d4cb-4740-93c8-4996dc90ec1b |
+-----------+--------------------------------------+```

And the results confirmed in the volume list for the destination project.

```
$ cinder list
+--------------------------------------+-----------+--------------+------+-------------+----------+-------------+
|                  ID                  |   Status  | Display Name | Size | Volume Type | Bootable | Attached to |
+--------------------------------------+-----------+--------------+------+-------------+----------+-------------+
| e20ed740-d4cb-4740-93c8-4996dc90ec1b | available |     None     |  1   |   standard  |  false   |             |
+--------------------------------------+-----------+--------------+------+-------------+----------+-------------+```
