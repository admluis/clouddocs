# IPv6

IPv6 provides many advantages over IPv4 with respect to the number of addresses available and more modern packet handling. Further information can be found in the [IPv6 training material](https://twiki.cern.ch/twiki/pub/IPv6/ResourceRepository/ipv6-day-training.pdf) and the [CERN ipv6](http://cern.ch/ipv6) web site.

This section describes how to configure IPv6 for your virtual machines concurrently with IPv4 (i.e. dual-stack).

The VM is configured using the LanDB fields meta data values. If *landb-ipv6ready* metadata value is set to true at VM boot time, the VM will be defined in the CERN Domain Name Server was being ready for IPv6. In this case, a client will try to contact the IPv6 address and fall back to the IPv4 one if there is no reply within a timeout. This would mean that client applications which can support IPv6 would use IPv6 in preference to ipv4. Only enable this option if the application software on the VM is able to support IPv6.

```
openstack server create --key-name lxplus --flavor m1.small --image 60366d54-63d8-4e58-97cf-9923bb21c538 --property landb-ipv6ready=true timipv6```

This will produce the table of data regarding the machine along with

```
| properties | landb-ipv6ready='true |
```
In the server list output, you can see both addresses

```
| ae7c1a37-e746-4c75-b7a5-dc53e131a8b0 | timipv6 | ACTIVE | CERN_NETWORK=128.142.167.10, 2001:1458:301:61::100:4 |```


The host command will then report the full set of addresses.

```
# host timipv6
timipv6.cern.ch has address 128.142.155.118
timipv6.cern.ch has IPv6 address 2001:1458:301:50::100:68```


On the machine itself, the interface details can be checked using ```
ip addr```
.

```
# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 16436 qdisc noqueue state UNKNOWN
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
    link/ether 02:16:3e:00:f5:fc brd ff:ff:ff:ff:ff:ff
    inet 128.142.155.118/16 brd 128.142.255.255 scope global eth0
    inet6 2001:1458:301:50::100:68/64 scope global
       valid_lft forever preferred_lft forever
    inet6 fe80::16:3eff:fe00:f5fc/64 scope link
       valid_lft forever preferred_lft forever```


