# LanDB network management

## Editing information in LanDB
For physical hardware, the owner of the machine can edit information in LanDB such as the main user or IP aliases.

For virtual machines, this information is managed by OpenStack and thus the entry in LanDB for the virtual machines is locked from editing. If you try to change the information, you will see the message 'The device MY-DEVICE-NAME is externally managed by a service provider'.

To set the fields in LanDB, you need to use the openstack tool to set the values as described in [Setting LanDB fields](http://clouddocs.web.cern.ch/clouddocs/using_openstack/properties.html).

## External access through firewall

The firewall rules can be set for virtual machines. The approaches that can be taken are

* Add the virtual machine to a LanDB set. This is useful if you have multiple machines which need the same firewall rules
* For Puppet managed machines, this is done through Hiera.
* Otherwise
   * use the [LanDB set](https://network.cern.ch/sc/fcgi/sc.fcgi?Action=SearchForSets) page.
   * Set the parameters for the device. By default, the CERN outer perimeter firewall blocks incoming access to systems on the CERN site. The detail of the policy is available on the [security pages](https://security.web.cern.ch/security/services/en/firewall.shtml). To request openings in the site firewall, please go to the Request Form at URL network.cern.ch/sc/fcgi/sc.fcgi?Action=FirewallRequest&InterfaceName=*MY-DEVICE-NAME*. Thus, to set the parameters for the machine *doctest143*, the URL https://network.cern.ch/sc/fcgi/sc.fcgi?Action=FirewallRequest&InterfaceName=doctest143 could be used.

## IP Address Ranges

For remote sites which have firewall rules, the range of IPs which could be allocated by the OpenStack framework is similar to the whole CERN network.

These addresses are listed at https://network.cern.ch/sc/fcgi/sc.fcgi?Action=GetFile&file=ip_networks.html
