# Clients

There are many different OpenStack client packages to interact with the CERN OpenStack clouds. The linux command line tools installed on lxplus are the reference configurations and will be maintained as the CERN cloud is upgraded.

However, there are other client options such as different operating systems or software development toolkits for automation which are described in the sections following.

There are a number of client options documented in the following sections. However, there may be further ones included the OpenStack documentation. The procedures have not been tested on the CERN private cloud but may work. See http://docs.openstack.org/user-guide/content/install_clients.html.

