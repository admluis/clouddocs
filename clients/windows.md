# Windows

This procedure has been tested on Windows 7 but may work for other versions also.

It is possible to use your Microsoft Windows machine to access the CERN private cloud. The requirements to do it are listed in the following points:

   * Check if you have installed Python 2.7 or newer (Python 3 is not currently supported by the OpenStack clients) and these additional components:
```
easy_install
pip
pywin32
pyOpenSSL 0.13```

This version prevents a known bug at  https://bugs.launchpad.net/pyopenssl/+bug/1234110.

If you have a Windows machine managed by CMF, you can add the CMF package "Python 2.7.5" that installs a Python environment with all the previous requirements included.

Once you have the Python environment configured, you can open a power shell window and install the client packages using pip installer with administrator privileges:

```
PS C:\Users\username> C:\Python27\Scripts\pip.exe install python-PROJECTclient```

Where PROJECT is the name of the OpenStack services: Nova, Glance, Cinder,...

For example, to install nova client, you must run:

```
PS C:\Users\username> C:\Python27\Scripts\pip.exe install python-novaclient
PS C:\Users\username> C:\Python27\Scripts\nova.exe --version
2.15.0```

You also have to download the PowerShell credentials script from the CERN OpenStack website:

   * Login on https://openstack.cern.ch
   * Choose the project to which you want to connect (e.g. Personal USERNAME).
   * Navigate to "Access & Security".
   * Select the "API Access" tab.
   * Press "Download PowerShell RC" button.

Before running the script, you should ensure that the execution policy in PowerShell is set to Unrestricted or RemoteSigned. To configure this parameter, you can run the following PowerShell commands with administrator privileges:

P```
S C:\Users\username> Get-ExecutionPolicy
Restricted
PS C:\Users\username> Set-ExecutionPolicy Unrestricted -Force```

Running the downloaded script from a powershell session will set all the required environment variables to your session. The script will also ask you the user password:

```
PS C:\Users\username> & '.\Personal USERNAME.ps1'
Please enter your OpenStack password: **********```

After running the credentials script, you can use the clients to manage your resources in the CERN private cloud:

```
PS C:\Users\username> C:\Python27\Scripts\nova.exe list
+--------------------------------------+-------------------+--------+------------+-------------+------------------------------+
| ID                                   | Name              | Status | Task State | Power State | Networks                     |
+--------------------------------------+-------------------+--------+------------+-------------+------------------------------+
| 00000000-0000-0000-0000-000000000000 | instance-name     | ACTIVE | None       | Running     | CERN_NETWORK=0.0.0.0         |
+--------------------------------------+-------------------+--------+------------+-------------+------------------------------+
PS C:\Users\username>```


