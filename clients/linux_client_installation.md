# Linux client installation

A full working client environment is configured on the lxplus service but there may be a need to install clients on other machines such as a personal laptop or an application server which needs to interact with OpenStack.

If you are developing an application, it is advised to use one of the software development toolkits rather than scripting around the command line tools.

**Note:** Currently, local installation of the OpenStack clients provides support for the OS_PASSWORD openrc method of authentication. 

## Puppet clients

If your machine uses Puppet, you can include the standard OpenStack clients by adding the following to your manifests.

```
  include 'openstack_clients'
```

## CentOS 7 and Scientific Linux 6

If you have a standalone Linux machine and would like to install the OpenStack clients manually, you can do the following using the RDO packages.

**Note:** This has been tested on CentOS 7 and SLC 6. Installations on Fedora should be followed up with the Fedora and RDO community if there are issues

### Centos 7

```
sudo yum update -y
sudo yum install -y https://rdo.fedorapeople.org/rdo-release.rpm
sudo yum install -y /usr/bin/{nova,glance,cinder,keystone,openstack}
```

### Scientific Linux 6

```
sudo yum update -y
sudo yum install -y https://repos.fedorapeople.org/repos/openstack/openstack-icehouse/rdo-release-icehouse-4.noarch.rpm
sudo yum install -y /usr/bin/{nova,glance,cinder,keystone,openstack}
```







