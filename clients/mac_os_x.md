# Mac OS X

Check if you have installed Python 2.6 or newer (currently, the clients do not support Python3).

```
python --version```


Install pip package through the package manager for your system:

```
sudo easy_install pip```


Run this command to install a client package for a desired project. Note that you must install each client separately.

```
sudo pip install python-PROJECTclient```

Where PROJECT is the project name for the OpenStack component (such as nova, glance, cinder or keystone) such as

```
sudo pip install python-novaclient```

The OpenStack environment variables should then be set up. This can be done using the steps at [Environment options](http://clouddocs.web.cern.ch/clouddocs/using_openstack/environment_options.html). It is recommended to use the Local Password script for a personally managed Mac.

The environment can then be sourced as usual.

```
source Personal\ USERNAME-openrc.sh```

