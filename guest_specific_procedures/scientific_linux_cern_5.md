# Scientific Linux CERN 5

While Scientific Linux CERN 5 is still supported, there are relatively few options for virtual guests in view of its age. Unless necessary, it is recommended to use a later version for production applications.

## Using tuned

The Red Hat tool tuned provides some good default performance tuning and configuration options.

## Lxplus-like environment

To install additional packages which would make your VM "like lxplus", please apply the following steps on your VM as root:

```
# yum groupinstall "Development Tools" -y
```

Note: this recipe is based on a [Service-Now Knowledge base article](https://cern.service-now.com/service-portal/article.do?n=KB0002752&s=lxplus%20vm). Please check this document for additional hints from the Lxplus service managers.

