# Guest specific procedures

Some operating systems require special procedures which are specific to the guest OS. These procedures are listed here by

   * Operating system
   * Release and versions

These procedures should be reviewed for the guests that are deployed with this operating system version.

In many cases, there are procedures using Puppet which can also be applied for multiple virtual machines. If you are deploying more than one server, it is recommended to review the options to use a [configuration management system](http://cern.ch/configdocs).
