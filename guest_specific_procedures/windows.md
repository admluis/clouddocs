# Windows guest specific procedures

## Configuring with CMF

Windows configurations can be automatically configured with CMF using the *cloud-init* plug in. See [Power Shell](http://clouddocs.web.cern.ch/clouddocs/using_openstack/contextualisation.html) section for the details.
