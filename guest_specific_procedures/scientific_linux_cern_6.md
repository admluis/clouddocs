# Scientific Linux CERN 6

These are specific procedures for Scientific Linux CERN 6. There are other procedures defined for [Scientific Linux CERN 5](http://clouddocs.web.cern.ch/clouddocs/guest_specific_procedures/scientific_linux_cern_5.html) and CentOS 7.

## Expanding the disk to fill available space

**Note:** This procedure requires a reboot of the virtual machine.

**Note:** This procedure is not required for Puppet managed machines as this is part of the ```ai-bs-vm``` procedure.

When a VM is initially created, the size of the filesystems and volumes on disk is defined by the VM image. The underlying disk size is defined by the flavor. Thus, it is common that the image has a size which does not use all the disk space and the file systems can be extended or new partitions created.

There is ongoing work to automate this procedure but this is not yet currently available. Details are available in the current restrictions section of the user guide.

**Note:** If you are using the configuration management tools and Puppet, this procedure is included into the initial virtual machine creation process.

### Default Image Configuration

The default image is a 10GB configuration as follows (from df -H)

```
$ df -H
Filesystem             Size   Used  Avail Use% Mounted on
/dev/mapper/VolGroup00-LogVol00
                       8.8G   1.9G   6.5G  22% /
tmpfs                  2.1G      0   2.1G   0% /dev/shm
/dev/vda1              204M    30M   164M  16% /boot
```


However, the underlying disk /dev/vda is bigger.

```
$ fdisk -l

Disk /dev/vda: 42.9 GB, 42949672960 bytes
16 heads, 63 sectors/track, 83220 cylinders
Units = cylinders of 1008 * 512 = 516096 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000408ec
```

### Option A - resize the / partition

**Note:** This procedure works for Scientific Linux CERN 6 only.

The following steps will resize the / partition so it occupies the remaining space. This procedure has only been tested on the standard cloud flavors (m1.*) so care should be taken on other flavors.

```
# growpart /dev/vda 2
CHANGED: partition=2 start=821248 old: size=20150272 end=20971520 new: size=83064512,end=83885760
```

**Note:** the error message "NOCHANGE: partition 2 is size XXXXXX. it cannot be grown" can be ignored if you already resized your instance before.
 
The VM should then be rebooted. Once rebooted,

```
# pvresize /dev/vda2
# lvextend -l +100%FREE /dev/mapper/VolGroup00-LogVol00
# resize2fs /dev/mapper/VolGroup00-LogVol00
```

### Option B - Create a new partition (such as /var)

A new filesystem could be created which uses the remaining space.  This procedure has only been tested on the standard cloud flavors (m1.*) so care should be taken on other flavors.

```
# growpart /dev/vda 2
```

Then reboot.

On reboot,

```
# pvresize /dev/vda2
# lvcreate -l+100%FREE -n varlog VolGroup00
# mkfs.ext4 /dev/VolGroup00/varlog
```

Add the following line to /etc/fstab

```
/dev/mapper/VolGroup00-varlog /var/log                       ext4    defaults        1 1
```

The move the old /var/log to /var/log.old and mount the new file system as follows

```
# service rsyslog stop
# mv /var/log /var/log.old
# mkdir /var/log
# chmod 755 /var/log
# mount -a
# service rsyslog start
```

This produces the result below

```
Filesystem             Size   Used  Avail Use% Mounted on
/dev/mapper/VolGroup00-LogVol00
                       8.8G   1.8G   6.6G  22% /
tmpfs                  2.1G      0   2.1G   0% /dev/shm
/dev/vda1              204M    30M   164M  16% /boot
/dev/mapper/VolGroup00-varlog
                        32G   181M    30G   1% /var/log
```

## Using tuned

The Red Hat tool tuned provides some good default performance tuning and configuration options.

## Lxplus-like environment

To install additional packages which would make your VM "like lxplus", please apply the following steps on your VM as root:

```
# yum groupinstall "Development Tools" -y
```

Note: this recipe is based on a [Service-Now Knowledge base article](https://cern.service-now.com/service-portal/article.do?n=KB0002752&s=lxplus%20vm). Please check this document for additional hints from the Lxplus service managers.

## Graphical interface

The provided SLC6 images will give you a Virtual Machine with a text console. If you require a graphical interface, please apply the following steps on your VM as root:

```
$ sudo yum groupinstall Desktop -y
```


update the file /etc/inittab to contain the line

```
id:5:initdefault:
```

then reboot the VM to start up X-Windows.