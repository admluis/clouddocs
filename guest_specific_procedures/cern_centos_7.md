# CERN CentOS 7

## Disk resizing

In CentOS 7, cloud-init will resize the system disk automatically so no additional work is required.

## Using tuned

The Red Hat tool tuned provides some good default performance tuning and configuration options. See [Performance Tuning](../advanced_topics/performance_tuning.md) for more details.

## Lxplus-like environment

To install additional packages which would make your VM "like lxplus", please apply the following steps on your VM as root:

```
# yum groupinstall "Development Tools" -y
```

Note: this recipe is based on a [Service-Now Knowledge base article](https://cern.service-now.com/service-portal/article.do?n=KB0002752&s=lxplus%20vm). Please check this document for additional hints from the Lxplus service managers.

## Graphical interface

The provided CC7 images will give you a Virtual Machine with a text console. If you require a graphical interface, please apply the following steps on your VM as root:

```
# yum groupinstall "GNOME Desktop" -y
```

Then, run 

```
# ln -sf /lib/systemd/system/graphical.target /etc/systemd/system/default.target
```

and reboot the VM to start up X-Windows.