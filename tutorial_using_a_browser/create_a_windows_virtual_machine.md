# Create a Windows virtual machine

In this tutorial, you will

1. Login to http://openstack.cern.ch
2. Create a Windows virtual machine and wait for it to build
3. Log in to the Windows virtual machine using Remote Desktop

## Logging in
After [Subscribing](http://clouddocs.web.cern.ch/clouddocs/tutorial_using_a_browser/subscribe_to_the_cloud_service.html), go to the OpenStack dashboard at [http://openstack.cern.ch](http://openstack.cern.ch).

The display should look as follows

![First Screen](http://cern.ch/clouddocs/images/firstscreen.png)

## Create a Windows 7 virtual machine

You can now proceed with creating a virtual machine.

* Select *instances* from the menu
* Select *launch instance* button
* enter the hostname, keypair, flavor and boot source as from image and the image into the panel. For the image, choose the latest by date of the Windows 7 images.
* Press launch

![Launch instance](http://cern.ch/clouddocs/images/launchwinvm.png)

## Accessing the graphical console

To following the installation, you can access the graphical console using the browser once the VM is in status BUILD.

The console is accessed by selecting the Instance Details for the machine and the tab 'Console'.

![Windows Console](http://cern.ch/clouddocs/images/windowsconsole.PNG)


## Remote desktop

When the build and the Windows installation steps have completed, you can access the console using the Windows Remote Desktop application.

![Login](http://cern.ch/clouddocs/images/winrdplogin.png)

![Check](http://cern.ch/clouddocs/images/winrdpcheck.png)

![Password](http://cern.ch/clouddocs/images/winrdppassword.png)

![Logged In](http://cern.ch/clouddocs/images/winrdploggedin.png)


