# Using a boot volume

For production servers which are not fully redundant and load balanced, it is advisable to use an external disk volume for the system disk rather than the local disk on the hypervisor (which becomes a single point of failure).

The following steps show how to create a virtual machine which boots from an external volume.

* Create a volume with source data from the image
* Launch a VM with that volume as the system disk

## Create a volume from image

![Create volume from an image](http://cern.ch/clouddocs/images/createvolumefromimage.png)

## Launch instance from existing volume

![Launch instance from an existing volume](http://cern.ch/clouddocs/images/launchinstancefromexistingvolume.png)

Remember to also set the keypair in access & security as in the section [Create virtual machine](http://clouddocs.web.cern.ch/clouddocs/tutorial_using_a_browser/create_a_virtual_machine.html).



