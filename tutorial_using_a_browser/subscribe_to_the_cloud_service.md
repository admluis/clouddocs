# Subscribe to the cloud service
These steps show how to subscribe to the CERN cloud service.

The summary of the steps are

1. Go to the CERN resources web site at http://cern.ch/resources
2. Select List Services
3. Select Cloud Infrastructure
4. Select Accounts
5. Select subscribe next to the account name you wish to subscribe
6. Confirm subscribe

On completion of these steps, you will receive an e-mail confirming that you can now log in. This confirmation normally takes around one hour.





