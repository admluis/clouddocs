# Create a linux virtual machine

In this tutorial, you will

1. Log in to http://openstack.cern.ch
2. Set up a keypair
2. Create a virtual machine

## Accessing the web site

After [Subscribing](http://clouddocs.web.cern.ch/clouddocs/tutorial_using_a_browser/subscribe_to_the_cloud_service.html), go to the OpenStack dashboard at [http://openstack.cern.ch](http://openstack.cern.ch).

The display should look as follows

![First Screen](http://cern.ch/clouddocs/images/firstscreen.png)

## Setting up a keypair

Keypairs are used to access virtual machines with ssh.

You can set up a keypair in a number of ways. The recommended approach is to use your credentials from an lxplus account with ssh.

* *access & security* from the menu
* Select the *import key pair* button
* enter lxplus for your keypair
* enter the contents of ~/.ssh/id_dsa.pub from lxplus as your key

![Import Key Pair](http://cern.ch/clouddocs/images/importkeypair.png)

## Creating a virtual machine

Once your key is defined, you can now proceed with creating a virtual machine.

* Select *instances* from the menu
* Select *launch instance* button
* enter
   * a hostname (you can choose it, such as timdoc143)
   * the keypair (such as lxplus created above)
   * the flavor (such as m1.small)
   * the boot source should be image
   * for the image, choose the latest by date of the SLC6 CERN Server images.

**DO NOT PRESS LAUNCH**.

* Select the *access & security* tab, enter your keypair (such as lxplus) and then press launch.

**NOW PRESS LAUNCH**.

![Launch instance](http://cern.ch/clouddocs/images/launchinstance.png)
![Launch Key Pair](http://cern.ch/clouddocs/images/launchkeypair.png)

## Follow the boot process
The boot process can be followed on the instances screen. Once the VM is in state ACTIVE, you should be able to open a console to follow it booting. Note: registration of your VM in the CERN Network database may take up to 10 minutes, and the first boot of the VM may take some time as well.

## Log in using ssh

Once the console shows the login prompt, you can log in with ssh as follows

```
$ ssh root@hostname
```




