## Contextualisation

The process of passing custom paramters to a virtual machine when it is created. This is often done using the cloud-init package.

## keypair

A secret key which is used to access private resources. This should be kept safe in a private directory with limited access and not shared.

## flavor

The description of the (virtual!) hardware of size of a virtual machine such as 1 virtual CPU, 40 GB of disk space and 4 GB memory.

## image

A reference copy of an operating system with optional additional software which can be used to create virtual machines. Typical examples would be a Linux Server or Windows 7 image.

## Infrastructure-as-a-Service

is a way of delivering computing infrastructure – servers, storage, network and operating systems – as an on-demand service. Rather than purchasing servers, software, datacenter space or network equipment, clients instead obtain those resources as a fully outsourced service on demand 

## Quota

A maximum amount of resources such as cores, memory or disk which can be requested for a given project.

## OpenStack

An open source cloud package used at CERN and many other organisations. See http://openstack.org for more details.

## Swift

This OpenStack component is not installed at CERN but in other clouds, it stores and retrieves unstructured data objects through the HTTP based APIs. Further, it is also fault tolerant due to its data replication and scale out architecture.

## Neutron

This OpenStack component is not installed at CERN but in other clouds, it is a pluggable, scalable and API-driven system for managing networks. OpenStack networking is useful for VLAN management, management of IP addresses to different VMs and management of firewalls using these components.

## Nova

OpenStack compute (codename: Nova) is the component which allows the user to create and manage virtual servers using the machine images. It is the brain of the Cloud. OpenStack compute provisions and manages large networks of virtual machines.

## Cinder

This component provides persistent block storage to running instances. The flexible architecture makes creating and managing block storage devices very easy.

## Keystone

This provides a central directory of users mapped to the OpenStack services. It is used to provide an authentication and authorization service for other OpenStack services.

## Glance

This provides the discovery, registration and delivery services for the disk and server images. It stores and retrieves the virtual machine disk image.

## Ceilometer

It monitors the usage of the Cloud services and decides the billing accordingly. This component is also used to decide the scalability and obtain the statistics regarding the usage.

## Horizon

This component provides a web-based portal to interact with all the underlying OpenStack services, such as NOVA, Neutron, etc.

## Heat

This component manages multiple Cloud applications through an OpenStack-native REST API and a CloudFormation-compatible Query API.

## snapshot

A snapshot provides a copy of a currently running VM or volume which can be stored into an external service such as Glance.

## Volume

Volumes are block storage devices that you attach to instances to enable persistent storage. You can attach a volume to a running instance or detach a volume and attach it to another instance at any time.

## tenant

A historical term for project.

## project

A project is a container used to group a set of resources such as virtual machines, volumes and images with the same access rights and quota.

## personal project

An allocation created for any user who signs up for the OpenStack cloud. It is intended for use for testing rather than production services (where a shared project should be used)

## shared project

A set of resources for a specific purpose such as a prodution service with a list of administrators who can manage the resources.

## e-group

A list of users managed by the CERN e-groups application

## unified client

The openstack command is referred to as the unified client since it managed multiple different components of OpenStack as opposed to the nova or cinder commands.

## Puppet

A connfiguration management system used at CERN. Details can be found at http://cern.ch/configdocs

## Icehouse

A release of OpenStack available in April 2014 and installed at CERN in October 2014.

## Havana

A release of OpenStack made available in October 2013 and installed at CERN during February 2014.

## Essex

An OpenStack release made available in Spring 2012

## Cactus

An OpenStack release available in Spring 2011.

## Grizzly

An OpenStack release in Spring 2013 and the first production release at CERN in July 2013.

## ephemeral

storage that defines a second disk as part of the virtual machine flavor

## bonnie++

A disk perfomance benchmark (http://www.coker.com.au/bonnie++/)

## LanDB

CERN's network management system at http://cern.ch/network.

## firewall

A filter of incoming and outgoing communication to a virtual machine or a network.

## X-Windows

A graphical interface for Unix and Linux systems

## IPv6

is a new way of addressing machines which is more flexible than IPv4.

## cloud-init

A method for contextualising a VM on first boot, such as installing software or configuring users.

## openrc

The shell profile to set up environment variables for accessing OpenStack. This is often sourced from the command line tools.

## EC2

The Elastic Compute protocol used by the Amazon public cloud which is partially emulated in OpenStack.

## X.509

A security mechanism to identify users and hosts using certificates, such as used on the WLCG.

## Kerberos

A security mechanism used to identify users such as used on the AFS file system and Active Directory.

## virtual machine

A virtual computer which runs on a virtualisation layer so that multiple virtual computers can run on one physical one.

## cloud

Cloud computing is a model for enabling convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction. 

## MacOS

The operating system running on Apple Mac computers.

## eucatools

A common command line tools package for using Amazon EC2 compatible clouds.

## euca2ools

A set of command line tools from Eucalyptus for interacting with Amazon EC2 compatible clouds. Details are at https://www.eucalyptus.com/docs/eucalyptus/4.0.1/index.html#shared/euca2ools_section.html

## lxplus

The CERN public login linux service

## SLC6

Scientific Linux CERN 6, the linux distribution used at CERN

## CC7

CERN CentOS 7, the latest linux distribution used at CERN based on CentOS.

## Power Shell

Windows scripting tool

## CMF

Computer Management Framework used by CERN for Windows PCs

## NSC

A group of related Windows PCs managed by CMF

## LVM

Linux Logical Volume Manager which is able to allocate logical disk space and resize partitions.

## sysprep

A tool to clean images of their local identities such as hostnames. It is needed when creating images without history of how they were built.

## CernVM

A microkernel image which uses CVMFS to store the application data. See http://cernvm.cern.ch/portal/openstack for more details.

## watchdog

A procedure to restart virtual machines automatically if stuck

## availability zone

a region of the computer centre which is distinct from another such that a failure in one availability zone is unlikely to affect another.

## RDO

A Linux community distribution of OpenStack.

## rescue

Recovering a system which is not able to boot cleanly.

## DNS

Domain Name Service, which maps from hostnames to TCP/IP addresses.

## hypervisor

The computer which hosts a number of virtual machines.

## tuned

A tool for automatically configuring Linux machines based on their roles.

## rebuild

Return a virtual machine to its base configuration.

## cernops

CERN operations source code repository for open source software on GitHub at http://github.com/cernops

## metadata

A set of key value pairs which can be associated with a VM, image, flavor or other objects.

## ntp

The internet network time protocol used to synchronise machines with a good time reference such as the ip-time-1 server at CERN.
